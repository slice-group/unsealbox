#Generado con Keppler.
<% if namespaced? -%>
require_dependency "<%= namespaced_file_path %>/application_controller"

<% end -%>
<% module_namespacing do -%>
class <%= controller_class_name %>Controller < ApplicationController  
  before_filter :authenticate_user!
  layout 'layouts/frontend/application'
  before_action :set_<%= singular_table_name %>, only: [:show, :edit, :update, :destroy]
  before_filter :set_subcategory

  # GET <%= route_url %>
  def index
    @contacts = Contact.where(user_id: current_user.id).order(first_name: :asc)
    @<%= singular_table_name %> = <%= singular_table_name.camelcase %>.new
    @<%= plural_table_name %> = <%= singular_table_name.camelcase %>.where(user_id: current_user.id).page params[:page]
  end

  # GET <%= route_url %>/1
  def show
  end

  # GET <%= route_url %>/new
  def new
    @<%= singular_table_name %> = <%= orm_class.build(class_name) %>
  end

  # GET <%= route_url %>/1/edit
  def edit
  end

  # POST <%= route_url %>
  def create
    @<%= singular_table_name %> = <%= orm_class.build(class_name, "#{singular_table_name}_params") %>
    @<%= singular_table_name %>.user_id = current_user.id
    respond_to do |format|
      if @<%= singular_table_name %>.save
        format.html { redirect_to <%= plural_table_name %>_path(@subcategory.category.permalink, @subcategory.permalink), notice: '<%= singular_table_name %> was successfully created.' }
        format.json { render :show, status: :created, location: @<%= singular_table_name %> }
      else
        format.html { render :new }
        format.json { render json: @<%= singular_table_name %>.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT <%= route_url %>/1
  def update
    case params[:commit]
    when "Save Information"
      respond_to do |format|
        if @<%= singular_table_name %>.update(restructuring_parameters(@<%= singular_table_name %>, <%= singular_table_name %>_params))
          format.html { redirect_to <%= plural_table_name %>_path(@subcategory.category.permalink, @subcategory.permalink), notice: '<%= singular_table_name %> was successfully updated.' }
          format.json { render :show, status: :ok, location: @<%= singular_table_name %> }
        else
          format.html { render :edit }
          format.json { render json: @<%= singular_table_name %>.errors, status: :unprocessable_entity }
        end
      end
    when "Select this contact"
      @<%= singular_table_name %>.update_attributes(contact_id: <%= singular_table_name %>_params[:contact_id])
      redirect_to <%= plural_table_name %>_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    when "Delete"
      @<%= singular_table_name %>.update_attributes(contact_id: nil)
      redirect_to <%= plural_table_name %>_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    end
  end

  # DELETE <%= route_url %>/1
  def destroy    
    @<%= singular_table_name %>.destroy
    respond_to do |format|
      format.html { redirect_to <%= plural_table_name %>_path(@subcategory.category.permalink, @subcategory.permalink), notice: '<%= singular_table_name %> was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_<%= singular_table_name %>
      @<%= singular_table_name %> = <%= orm_class.find(class_name, "params[:id]") %>
    end

    # Only allow a trusted parameter "white list" through.
    def <%= "#{singular_table_name}_params" %>
      <%- if attributes_names.empty? -%>
      params[:<%= singular_table_name %>]
      <%- else -%>
      params.require(:<%= singular_table_name %>).permit(<%= attributes_names.map { |name| ":#{name}" }.join(', ') %>, contact_attributes: [:id, :first_name, :middle_name, :last_name, :birth_date, :phone_number, :email, :address, :city, :state, :zip, :country, :notes])
      <%- end -%>
    end
end
<% end -%>
