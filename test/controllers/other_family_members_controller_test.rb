require 'test_helper'

class OtherFamilyMembersControllerTest < ActionController::TestCase
  setup do
    @other_family_member = other_family_members(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:other_family_members)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create other_family_member" do
    assert_difference('OtherFamilyMember.count') do
      post :create, other_family_member: { contact_id: @other_family_member.contact_id, notes: @other_family_member.notes, related_to_you: @other_family_member.related_to_you, user_id: @other_family_member.user_id }
    end

    assert_redirected_to other_family_member_path(assigns(:other_family_member))
  end

  test "should show other_family_member" do
    get :show, id: @other_family_member
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @other_family_member
    assert_response :success
  end

  test "should update other_family_member" do
    patch :update, id: @other_family_member, other_family_member: { contact_id: @other_family_member.contact_id, notes: @other_family_member.notes, related_to_you: @other_family_member.related_to_you, user_id: @other_family_member.user_id }
    assert_redirected_to other_family_member_path(assigns(:other_family_member))
  end

  test "should destroy other_family_member" do
    assert_difference('OtherFamilyMember.count', -1) do
      delete :destroy, id: @other_family_member
    end

    assert_redirected_to other_family_members_path
  end
end
