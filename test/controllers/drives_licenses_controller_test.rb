require 'test_helper'

class DrivesLicensesControllerTest < ActionController::TestCase
  setup do
    @drives_license = drives_licenses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:drives_licenses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create drives_license" do
    assert_difference('DrivesLicense.count') do
      post :create, drives_license: { contact_id: @drives_license.contact_id, country: @drives_license.country, expiration_date: @drives_license.expiration_date, location: @drives_license.location, notes: @drives_license.notes, number: @drives_license.number, state: @drives_license.state, string: @drives_license.string, user_id: @drives_license.user_id }
    end

    assert_redirected_to drives_license_path(assigns(:drives_license))
  end

  test "should show drives_license" do
    get :show, id: @drives_license
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @drives_license
    assert_response :success
  end

  test "should update drives_license" do
    patch :update, id: @drives_license, drives_license: { contact_id: @drives_license.contact_id, country: @drives_license.country, expiration_date: @drives_license.expiration_date, location: @drives_license.location, notes: @drives_license.notes, number: @drives_license.number, state: @drives_license.state, string: @drives_license.string, user_id: @drives_license.user_id }
    assert_redirected_to drives_license_path(assigns(:drives_license))
  end

  test "should destroy drives_license" do
    assert_difference('DrivesLicense.count', -1) do
      delete :destroy, id: @drives_license
    end

    assert_redirected_to drives_licenses_path
  end
end
