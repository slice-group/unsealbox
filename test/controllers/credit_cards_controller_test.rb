require 'test_helper'

class CreditCardsControllerTest < ActionController::TestCase
  setup do
    @credit_card = credit_cards(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:credit_cards)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create credit_card" do
    assert_difference('CreditCard.count') do
      post :create, credit_card: { card_number: @credit_card.card_number, contact_id: @credit_card.contact_id, expiration_date: @credit_card.expiration_date, note_and_instructions: @credit_card.note_and_instructions, type_of_card: @credit_card.type_of_card, user_id: @credit_card.user_id, what_type_of_card_it: @credit_card.what_type_of_card_it }
    end

    assert_redirected_to credit_card_path(assigns(:credit_card))
  end

  test "should show credit_card" do
    get :show, id: @credit_card
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @credit_card
    assert_response :success
  end

  test "should update credit_card" do
    patch :update, id: @credit_card, credit_card: { card_number: @credit_card.card_number, contact_id: @credit_card.contact_id, expiration_date: @credit_card.expiration_date, note_and_instructions: @credit_card.note_and_instructions, type_of_card: @credit_card.type_of_card, user_id: @credit_card.user_id, what_type_of_card_it: @credit_card.what_type_of_card_it }
    assert_redirected_to credit_card_path(assigns(:credit_card))
  end

  test "should destroy credit_card" do
    assert_difference('CreditCard.count', -1) do
      delete :destroy, id: @credit_card
    end

    assert_redirected_to credit_cards_path
  end
end
