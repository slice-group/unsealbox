require 'test_helper'

class NewslattersControllerTest < ActionController::TestCase
  setup do
    @newslatter = newslatters(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:newslatters)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create newslatter" do
    assert_difference('Newslatter.count') do
      post :create, newslatter: { email: @newslatter.email }
    end

    assert_redirected_to newslatter_path(assigns(:newslatter))
  end

  test "should show newslatter" do
    get :show, id: @newslatter
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @newslatter
    assert_response :success
  end

  test "should update newslatter" do
    patch :update, id: @newslatter, newslatter: { email: @newslatter.email }
    assert_redirected_to newslatter_path(assigns(:newslatter))
  end

  test "should destroy newslatter" do
    assert_difference('Newslatter.count', -1) do
      delete :destroy, id: @newslatter
    end

    assert_redirected_to newslatters_path
  end
end
