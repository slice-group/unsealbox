require 'test_helper'

class PublicLettersControllerTest < ActionController::TestCase
  setup do
    @public_letter = public_letters(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:public_letters)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create public_letter" do
    assert_difference('PublicLetter.count') do
      post :create, public_letter: { body_letter: @public_letter.body_letter, notes: @public_letter.notes, user_id: @public_letter.user_id }
    end

    assert_redirected_to public_letter_path(assigns(:public_letter))
  end

  test "should show public_letter" do
    get :show, id: @public_letter
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @public_letter
    assert_response :success
  end

  test "should update public_letter" do
    patch :update, id: @public_letter, public_letter: { body_letter: @public_letter.body_letter, notes: @public_letter.notes, user_id: @public_letter.user_id }
    assert_redirected_to public_letter_path(assigns(:public_letter))
  end

  test "should destroy public_letter" do
    assert_difference('PublicLetter.count', -1) do
      delete :destroy, id: @public_letter
    end

    assert_redirected_to public_letters_path
  end
end
