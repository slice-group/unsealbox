require 'test_helper'

class LifeInsurancesControllerTest < ActionController::TestCase
  setup do
    @life_insurance = life_insurances(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:life_insurances)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create life_insurance" do
    assert_difference('LifeInsurance.count') do
      post :create, life_insurance: { contact_id: @life_insurance.contact_id, location_of_original_policy: @life_insurance.location_of_original_policy, name_of_insurance_company: @life_insurance.name_of_insurance_company, notes: @life_insurance.notes, policy_expiration_date: @life_insurance.policy_expiration_date, policy_start_date: @life_insurance.policy_start_date, type_of_policy: @life_insurance.type_of_policy, user_id: @life_insurance.user_id, value_of_death_benefit: @life_insurance.value_of_death_benefit, what_is_the_policy_number: @life_insurance.what_is_the_policy_number }
    end

    assert_redirected_to life_insurance_path(assigns(:life_insurance))
  end

  test "should show life_insurance" do
    get :show, id: @life_insurance
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @life_insurance
    assert_response :success
  end

  test "should update life_insurance" do
    patch :update, id: @life_insurance, life_insurance: { contact_id: @life_insurance.contact_id, location_of_original_policy: @life_insurance.location_of_original_policy, name_of_insurance_company: @life_insurance.name_of_insurance_company, notes: @life_insurance.notes, policy_expiration_date: @life_insurance.policy_expiration_date, policy_start_date: @life_insurance.policy_start_date, type_of_policy: @life_insurance.type_of_policy, user_id: @life_insurance.user_id, value_of_death_benefit: @life_insurance.value_of_death_benefit, what_is_the_policy_number: @life_insurance.what_is_the_policy_number }
    assert_redirected_to life_insurance_path(assigns(:life_insurance))
  end

  test "should destroy life_insurance" do
    assert_difference('LifeInsurance.count', -1) do
      delete :destroy, id: @life_insurance
    end

    assert_redirected_to life_insurances_path
  end
end
