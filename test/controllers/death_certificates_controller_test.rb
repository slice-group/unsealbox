require 'test_helper'

class DeathCertificatesControllerTest < ActionController::TestCase
  setup do
    @death_certificate = death_certificates(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:death_certificates)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create death_certificate" do
    assert_difference('DeathCertificate.count') do
      post :create, death_certificate: { document: @death_certificate.document, responsability_id: @death_certificate.responsability_id }
    end

    assert_redirected_to death_certificate_path(assigns(:death_certificate))
  end

  test "should show death_certificate" do
    get :show, id: @death_certificate
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @death_certificate
    assert_response :success
  end

  test "should update death_certificate" do
    patch :update, id: @death_certificate, death_certificate: { document: @death_certificate.document, responsability_id: @death_certificate.responsability_id }
    assert_redirected_to death_certificate_path(assigns(:death_certificate))
  end

  test "should destroy death_certificate" do
    assert_difference('DeathCertificate.count', -1) do
      delete :destroy, id: @death_certificate
    end

    assert_redirected_to death_certificates_path
  end
end
