require 'test_helper'

class EmergenciesControllerTest < ActionController::TestCase
  setup do
    @emergency = emergencies(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:emergencies)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create emergency" do
    assert_difference('Emergency.count') do
      post :create, emergency: { contact_id: @emergency.contact_id, note: @emergency.note, relationship: @emergency.relationship, user_id: @emergency.user_id }
    end

    assert_redirected_to emergency_path(assigns(:emergency))
  end

  test "should show emergency" do
    get :show, id: @emergency
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @emergency
    assert_response :success
  end

  test "should update emergency" do
    patch :update, id: @emergency, emergency: { contact_id: @emergency.contact_id, note: @emergency.note, relationship: @emergency.relationship, user_id: @emergency.user_id }
    assert_redirected_to emergency_path(assigns(:emergency))
  end

  test "should destroy emergency" do
    assert_difference('Emergency.count', -1) do
      delete :destroy, id: @emergency
    end

    assert_redirected_to emergencies_path
  end
end
