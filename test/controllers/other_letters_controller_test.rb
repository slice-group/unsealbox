require 'test_helper'

class OtherLettersControllerTest < ActionController::TestCase
  setup do
    @other_letter = other_letters(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:other_letters)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create other_letter" do
    assert_difference('OtherLetter.count') do
      post :create, other_letter: { body_letter: @other_letter.body_letter, contact_id: @other_letter.contact_id, notes: @other_letter.notes, user_id: @other_letter.user_id }
    end

    assert_redirected_to other_letter_path(assigns(:other_letter))
  end

  test "should show other_letter" do
    get :show, id: @other_letter
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @other_letter
    assert_response :success
  end

  test "should update other_letter" do
    patch :update, id: @other_letter, other_letter: { body_letter: @other_letter.body_letter, contact_id: @other_letter.contact_id, notes: @other_letter.notes, user_id: @other_letter.user_id }
    assert_redirected_to other_letter_path(assigns(:other_letter))
  end

  test "should destroy other_letter" do
    assert_difference('OtherLetter.count', -1) do
      delete :destroy, id: @other_letter
    end

    assert_redirected_to other_letters_path
  end
end
