require 'test_helper'

class DayCarePersonnelsControllerTest < ActionController::TestCase
  setup do
    @day_care_personnel = day_care_personnels(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:day_care_personnels)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create day_care_personnel" do
    assert_difference('DayCarePersonnel.count') do
      post :create, day_care_personnel: { contact_id: @day_care_personnel.contact_id, note_and_instructions: @day_care_personnel.note_and_instructions, type_of_day_care: @day_care_personnel.type_of_day_care, user_id: @day_care_personnel.user_id }
    end

    assert_redirected_to day_care_personnel_path(assigns(:day_care_personnel))
  end

  test "should show day_care_personnel" do
    get :show, id: @day_care_personnel
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @day_care_personnel
    assert_response :success
  end

  test "should update day_care_personnel" do
    patch :update, id: @day_care_personnel, day_care_personnel: { contact_id: @day_care_personnel.contact_id, note_and_instructions: @day_care_personnel.note_and_instructions, type_of_day_care: @day_care_personnel.type_of_day_care, user_id: @day_care_personnel.user_id }
    assert_redirected_to day_care_personnel_path(assigns(:day_care_personnel))
  end

  test "should destroy day_care_personnel" do
    assert_difference('DayCarePersonnel.count', -1) do
      delete :destroy, id: @day_care_personnel
    end

    assert_redirected_to day_care_personnels_path
  end
end
