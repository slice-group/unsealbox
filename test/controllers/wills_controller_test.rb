require 'test_helper'

class WillsControllerTest < ActionController::TestCase
  setup do
    @will = wills(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:wills)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create will" do
    assert_difference('Will.count') do
      post :create, will: { contact_id: @will.contact_id, note_and_instructions: @will.note_and_instructions, user_id: @will.user_id, when_was_it_last_update: @will.when_was_it_last_update, where_do_you_keep_notarized: @will.where_do_you_keep_notarized }
    end

    assert_redirected_to will_path(assigns(:will))
  end

  test "should show will" do
    get :show, id: @will
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @will
    assert_response :success
  end

  test "should update will" do
    patch :update, id: @will, will: { contact_id: @will.contact_id, note_and_instructions: @will.note_and_instructions, user_id: @will.user_id, when_was_it_last_update: @will.when_was_it_last_update, where_do_you_keep_notarized: @will.where_do_you_keep_notarized }
    assert_redirected_to will_path(assigns(:will))
  end

  test "should destroy will" do
    assert_difference('Will.count', -1) do
      delete :destroy, id: @will
    end

    assert_redirected_to wills_path
  end
end
