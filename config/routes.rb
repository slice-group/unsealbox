Rails.application.routes.draw do   

  resources :deputies, only: [:index, :new, :create, :destroy] do
    get 'responsibilities', action: :responsabilities, on: :collection, as: :responsabilities
    put 'update-access', action: :update, on: :collection, as: :update_access
    put ':id/upload-document', action: :upload_document, on: :collection, as: :upload_document
  end

  resources :contacts

  root to: 'frontend#index'
  
  get "/my-tasks/:category_permalink", to: "frontend#mytasks", as: "myunsealbox"
  get "/my-box", to: "frontend#myboxes", as: "myplan"
  get "/category/:category_id/get-subcategories", to: "frontend#get_subcategories", as: :get_subcategories
  get "subcategory/:subcategory_id/:model_name/:model_id/contacts/:contact_id/:switch", to: "frontend#get_contact", as: :get_contact
  get "change_status/:subcategory_id", to: "frontend#change_status", as: :change_status
  get "my-boxes-information/:objects", to: "frontend#get_my_plan_information", as: :my_plan_information
  get "my-boxes-report", to: "frontend#my_plan_report", as: :my_plan_report
  get "shared-boxes-report/:user_permalink", to: "frontend#shared_plan_report", as: :shared_plan_report
  get "users/:user_permalink/shared", to: "frontend#shared_plan", as: :shared_plan
  get "shared-information/:objects/:user_permalink", to: "frontend#shared_information", as: :shared_information

  devise_for :users, skip: KepplerConfiguration.skip_module_devise

  resources :admin, only: :index

  # -- contact_informations --
  scope "/:category_permalink/:subcategory_permalink" do
    resources :contact_informations
    resources :doctors
    resources :spouses
    resources :children
    resources :emergencies
    resources :advisors
    resources :life_insurances
    resources :attorneys
    resources :executors
    resources :guardians
    resources :life_insurences
    resources :wills
    resources :beneficiaries
    resources :obituaries
    resources :services
    resources :public_letters
    resources :other_letters
    resources :passports
    resources :drives_licenses
    resources :contact_informations
    resources :other_family_members
    resources :accounts
    resources :credit_cards
    resources :day_care_personnels
  end
  # -- contact_informations --


  scope :admin do
   
  	resources :users do 
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
    end

    resources :categories do 
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
    end

    resources :subcategories do 
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
    end

    resources :newslatters do 
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
    end

    resources :death_certificates do 
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
    end

    resources :landing_pages do
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
    end
  end

  #errors
  match '/403', to: 'errors#not_authorized', via: :all, as: :not_authorized
  match '/404', to: 'errors#not_found', via: :all
  match '/422', to: 'errors#unprocessable', via: :all
  match '/500', to: 'errors#internal_server_error', via: :all


  #dashboard
  mount KepplerGaDashboard::Engine, :at => '/', as: 'dashboard'


end
