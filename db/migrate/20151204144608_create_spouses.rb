class CreateSpouses < ActiveRecord::Migration
  def change
    create_table :spouses do |t|
      t.string :status
      t.text :note
      t.belongs_to :user, index: true
      t.belongs_to :contact

      t.timestamps null: false
    end
    add_foreign_key :spouses, :users
  end
end
