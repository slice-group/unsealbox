class CreateAdvisors < ActiveRecord::Migration
  def change
    create_table :advisors do |t|
      t.string :type_of_advisor
      t.text :notes
      t.belongs_to :user, index: true
      t.belongs_to :contact

      t.timestamps null: false
    end
    add_foreign_key :advisors, :users
  end
end
