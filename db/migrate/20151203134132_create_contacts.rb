class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :first_name
      t.string :middle_name
      t.string :last_name
      t.date :birth_date
      t.string :phone_number
      t.string :email
      t.text :address
      t.string :city
      t.string :state
      t.string :zip
      t.string :country
      t.string :notes
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
