class CreateResponsabilitiesSubcategories < ActiveRecord::Migration
  def change
    create_table :responsabilities_subcategories do |t|
      t.references :responsability, index: true
      t.references :subcategory, index: true

      t.timestamps null: false
    end
    add_foreign_key :responsabilities_subcategories, :responsabilities
    add_foreign_key :responsabilities_subcategories, :subcategories
  end
end
