class CreateLandingPages < ActiveRecord::Migration
  def change
    create_table :landing_pages do |t|
      t.string :title
      t.string :background
      t.text :text_one
      t.text :text_two
      t.text :meta_tags
      t.string :google_analytics_track
      t.text :google_adwords_script

      t.timestamps null: false
    end
  end
end
