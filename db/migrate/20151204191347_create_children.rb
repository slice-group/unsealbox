class CreateChildren < ActiveRecord::Migration
  def change
    create_table :children do |t|
      t.string :school
      t.text :health
      t.text :dentary
      t.text :note
      t.belongs_to :contact
      t.belongs_to :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :children, :users
  end
end
