class CreateOtherFamilyMembers < ActiveRecord::Migration
  def change
    create_table :other_family_members do |t|
      t.string :related_to_you
      t.text :notes
      t.belongs_to :user, index: true
      t.integer :contact_id

      t.timestamps null: false
    end
    add_foreign_key :other_family_members, :users
  end
end
