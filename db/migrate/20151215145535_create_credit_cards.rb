class CreateCreditCards < ActiveRecord::Migration
  def change
    create_table :credit_cards do |t|
      t.string :type_of_card
      t.string :what_type_of_card_it
      t.string :card_number
      t.date :expiration_date
      t.text :note_and_instructions
      t.belongs_to :contact
      t.belongs_to :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :credit_cards, :users
  end
end
