class CreateNewslatters < ActiveRecord::Migration
  def change
    create_table :newslatters do |t|
      t.string :email

      t.timestamps null: false
    end
  end
end
