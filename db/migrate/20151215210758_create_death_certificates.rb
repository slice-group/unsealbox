class CreateDeathCertificates < ActiveRecord::Migration
  def change
    create_table :death_certificates do |t|
      t.integer :responsability_id
      t.string :document
      t.integer :death_notification_id
      t.date :death_date

      t.timestamps null: false
    end
  end
end
