class CreatePublicLetters < ActiveRecord::Migration
  def change
    create_table :public_letters do |t|
      t.text :body_letter
      t.text :notes
      t.belongs_to :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :public_letters, :users
  end
end
