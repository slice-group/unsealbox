class CreateLifeInsurances < ActiveRecord::Migration
  def change
    create_table :life_insurances do |t|
      t.string :name_of_insurance_company
      t.string :location_of_original_policy
      t.string :type_of_policy
      t.string :what_is_the_policy_number
      t.string :policy_start_date
      t.string :policy_expiration_date
      t.string :value_of_death_benefit
      t.text :notes
      t.belongs_to :user, index: true
      t.integer :contact_id

      t.timestamps null: false
    end
    add_foreign_key :life_insurances, :users
  end
end
