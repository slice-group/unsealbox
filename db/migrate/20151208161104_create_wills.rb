class CreateWills < ActiveRecord::Migration
  def change
    create_table :wills do |t|
      t.string :where_do_you_keep_notarized
      t.string :when_was_it_last_update
      t.string :where_is_it
      t.text :note_and_instructions
      t.integer :contact_id
      t.belongs_to :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :wills, :users
  end
end
