class CreateExecutors < ActiveRecord::Migration
  def change
    create_table :executors do |t|
      t.string :type_of_executor
      t.text :note_and_instructions_for_executor
      t.integer :contact_id
      t.belongs_to :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :executors, :users
  end
end
