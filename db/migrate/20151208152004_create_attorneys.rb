class CreateAttorneys < ActiveRecord::Migration
  def change
    create_table :attorneys do |t|
      t.string :type_of_attorney
      t.text :note_and_instructions
      t.belongs_to :user, index: true
      t.integer :contact_id

      t.timestamps null: false
    end
    add_foreign_key :attorneys, :users
  end
end
