class CreatePassports < ActiveRecord::Migration
  def change
    create_table :passports do |t|
      t.string :country_of_issue
      t.string :number
      t.string :expiration_date
      t.string :location
      t.text :notes
      t.integer :contact_id
      t.belongs_to :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :passports, :users
  end
end
