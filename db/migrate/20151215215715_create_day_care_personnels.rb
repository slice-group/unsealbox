class CreateDayCarePersonnels < ActiveRecord::Migration
  def change
    create_table :day_care_personnels do |t|
      t.string :type_of_day_care
      t.text :note_and_instructions
      t.belongs_to :contact
      t.belongs_to :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :day_care_personnels, :users
  end
end
