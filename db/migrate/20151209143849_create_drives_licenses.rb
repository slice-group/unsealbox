class CreateDrivesLicenses < ActiveRecord::Migration
  def change
    create_table :drives_licenses do |t|
      t.string :number
      t.string :country
      t.string :state
      t.string :expiration_date
      t.string :string
      t.string :location
      t.text :notes
      t.integer :contact_id
      t.belongs_to :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :drives_licenses, :users
  end
end
