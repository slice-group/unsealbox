class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.string :type_of_account
      t.string :name_of_finalcial_institution
      t.string :account_number
      t.text :note_and_instructions
      t.belongs_to :contact
      t.belongs_to :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :accounts, :users
  end
end
