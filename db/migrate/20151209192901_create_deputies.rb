class CreateDeputies < ActiveRecord::Migration
  def change
    create_table :deputies do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :death_notification_id

      t.timestamps null: false
    end
  end
end
