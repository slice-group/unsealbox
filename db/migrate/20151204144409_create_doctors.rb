class CreateDoctors < ActiveRecord::Migration
  def change
    create_table :doctors do |t|
      t.string :type_of_doctor
      t.text :notes
      t.belongs_to :contact
      t.belongs_to :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :doctors, :users
  end
end
