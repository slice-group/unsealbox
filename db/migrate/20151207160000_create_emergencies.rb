class CreateEmergencies < ActiveRecord::Migration
  def change
    create_table :emergencies do |t|
      t.string :relationship
      t.text :note
      t.belongs_to :contact
      t.belongs_to :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :emergencies, :users
  end
end
