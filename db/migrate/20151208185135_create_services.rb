class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :type_of_service
      t.string :location
      t.text :note_and_instructions
      t.belongs_to :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :services, :users
  end
end
