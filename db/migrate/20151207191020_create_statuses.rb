class CreateStatuses < ActiveRecord::Migration
  def change
    create_table :statuses do |t|
      t.belongs_to :subcategory, index: true
      t.belongs_to :user, index: true
      t.string :name, null: false, default: "clear"

      t.timestamps null: false
    end
    add_foreign_key :statuses, :subcategories
    add_foreign_key :statuses, :users
  end
end
