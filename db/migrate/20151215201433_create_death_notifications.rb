class CreateDeathNotifications < ActiveRecord::Migration
  def change
    create_table :death_notifications do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
