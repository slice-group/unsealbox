class CreateResponsabilities < ActiveRecord::Migration
  def change
    create_table :responsabilities do |t|
      t.integer :deputy_id
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
