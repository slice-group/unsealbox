class CreateGuardians < ActiveRecord::Migration
  def change
    create_table :guardians do |t|
      t.string :type_of_guardian
      t.text :note_and_instructions_for_guardians
      t.integer :contact_id
      t.belongs_to :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :guardians, :users
  end
end
