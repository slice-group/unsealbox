class CreateOtherLetters < ActiveRecord::Migration
  def change
    create_table :other_letters do |t|
      t.text :body_letter
      t.text :notes
      t.belongs_to :user, index: true
      t.integer :contact_id

      t.timestamps null: false
    end
    add_foreign_key :other_letters, :users
  end
end
