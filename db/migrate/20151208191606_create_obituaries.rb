class CreateObituaries < ActiveRecord::Migration
  def change
    create_table :obituaries do |t|
      t.text :memories_and_accomplishments
      t.text :note_and_instructions
      t.belongs_to :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :obituaries, :users
  end
end
