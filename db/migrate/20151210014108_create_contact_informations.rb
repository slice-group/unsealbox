class CreateContactInformations < ActiveRecord::Migration
  def change
    create_table :contact_informations do |t|
      t.belongs_to :user, index: true
      t.integer :contact_id

      t.timestamps null: false
    end
    add_foreign_key :contact_informations, :users
  end
end
