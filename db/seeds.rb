# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#user = CreateAdminService.new.call
#puts 'CREATED ADMIN USER: ' << user.email

[:admin, :client].each do |name|
	Role.create name: name
	puts "Rol: #{name} creado"
end


["my life", "health & medical", "financial", "legal", "post life"].each do |name|
	Category.create name: name
	puts "Category: #{name} creado"
end

["About me", "My family", "Emergency contacts"].each do |name|
	Subcategory.create name: name, category_id: 1
end

["Doctors", "Medical assistants"].each do |name|
	Subcategory.create name: name, category_id: 2
end

["Financial advisor", "Bank Information", "My life insurance"].each do |name|
	Subcategory.create name: name, category_id: 3
end

["Attorneys", "Will"].each do |name|
	Subcategory.create name: name, category_id: 4
end

["Funeral preference", "Letter to family & friends"].each do |name|
	Subcategory.create name: name, category_id: 5
end

["after i get in coma", "after i depart life", "after my funeral", "1 month after my funeral"].each do |notification|
	DeathNotification.create name: notification
end



User.create name: "Admin", email: "admin@unsealbox.com", password: "12345678", password_confirmation: "12345678", role_ids: "1"
puts "Admin ha sido creado"

User.create name: "Gabriel", email: "gabriel@slicegroup.com", password: "12345678", password_confirmation: "12345678", role_ids: "2"
puts "Gabriel ha sido creado"

User.create name: "Luis", email: "luis@slicegroup.com", password: "12345678", password_confirmation: "12345678", role_ids: "2"
puts "Luis ha sido creado"