# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151217182316) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounts", force: :cascade do |t|
    t.string   "type_of_account"
    t.string   "name_of_finalcial_institution"
    t.string   "account_number"
    t.text     "note_and_instructions"
    t.integer  "contact_id"
    t.integer  "user_id"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "accounts", ["user_id"], name: "index_accounts_on_user_id", using: :btree

  create_table "advisors", force: :cascade do |t|
    t.string   "type_of_advisor"
    t.text     "notes"
    t.integer  "user_id"
    t.integer  "contact_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "advisors", ["user_id"], name: "index_advisors_on_user_id", using: :btree

  create_table "attorneys", force: :cascade do |t|
    t.string   "type_of_attorney"
    t.text     "note_and_instructions"
    t.integer  "user_id"
    t.integer  "contact_id"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "attorneys", ["user_id"], name: "index_attorneys_on_user_id", using: :btree

  create_table "beneficiaries", force: :cascade do |t|
    t.string   "name"
    t.text     "notes"
    t.integer  "user_id"
    t.integer  "contact_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "beneficiaries", ["user_id"], name: "index_beneficiaries_on_user_id", using: :btree

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.string   "permalink"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "children", force: :cascade do |t|
    t.string   "school"
    t.text     "health"
    t.text     "dentary"
    t.text     "note"
    t.integer  "contact_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "children", ["user_id"], name: "index_children_on_user_id", using: :btree

  create_table "contact_informations", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "contact_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "contact_informations", ["user_id"], name: "index_contact_informations_on_user_id", using: :btree

  create_table "contacts", force: :cascade do |t|
    t.string   "first_name"
    t.string   "middle_name"
    t.string   "last_name"
    t.date     "birth_date"
    t.string   "phone_number"
    t.string   "email"
    t.text     "address"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.string   "country"
    t.string   "notes"
    t.integer  "user_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "credit_cards", force: :cascade do |t|
    t.string   "type_of_card"
    t.string   "what_type_of_card_it"
    t.string   "card_number"
    t.date     "expiration_date"
    t.text     "note_and_instructions"
    t.integer  "contact_id"
    t.integer  "user_id"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "credit_cards", ["user_id"], name: "index_credit_cards_on_user_id", using: :btree

  create_table "day_care_personnels", force: :cascade do |t|
    t.string   "type_of_day_care"
    t.text     "note_and_instructions"
    t.integer  "contact_id"
    t.integer  "user_id"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "day_care_personnels", ["user_id"], name: "index_day_care_personnels_on_user_id", using: :btree

  create_table "death_certificates", force: :cascade do |t|
    t.integer  "responsability_id"
    t.string   "document"
    t.integer  "death_notification_id"
    t.date     "death_date"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "death_notifications", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "deputies", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "death_notification_id"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "doctors", force: :cascade do |t|
    t.string   "type_of_doctor"
    t.text     "notes"
    t.integer  "contact_id"
    t.integer  "user_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "doctors", ["user_id"], name: "index_doctors_on_user_id", using: :btree

  create_table "drives_licenses", force: :cascade do |t|
    t.string   "number"
    t.string   "country"
    t.string   "state"
    t.string   "expiration_date"
    t.string   "string"
    t.string   "location"
    t.text     "notes"
    t.integer  "contact_id"
    t.integer  "user_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "drives_licenses", ["user_id"], name: "index_drives_licenses_on_user_id", using: :btree

  create_table "emergencies", force: :cascade do |t|
    t.string   "relationship"
    t.text     "note"
    t.integer  "contact_id"
    t.integer  "user_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "emergencies", ["user_id"], name: "index_emergencies_on_user_id", using: :btree

  create_table "executors", force: :cascade do |t|
    t.string   "type_of_executor"
    t.text     "note_and_instructions_for_executor"
    t.integer  "contact_id"
    t.integer  "user_id"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  add_index "executors", ["user_id"], name: "index_executors_on_user_id", using: :btree

  create_table "guardians", force: :cascade do |t|
    t.string   "type_of_guardian"
    t.text     "note_and_instructions_for_guardians"
    t.integer  "contact_id"
    t.integer  "user_id"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "guardians", ["user_id"], name: "index_guardians_on_user_id", using: :btree

  create_table "landing_pages", force: :cascade do |t|
    t.string   "title"
    t.string   "background"
    t.text     "text_one"
    t.text     "text_two"
    t.text     "meta_tags"
    t.string   "google_analytics_track"
    t.text     "google_adwords_script"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "life_insurances", force: :cascade do |t|
    t.string   "name_of_insurance_company"
    t.string   "location_of_original_policy"
    t.string   "type_of_policy"
    t.string   "what_is_the_policy_number"
    t.string   "policy_start_date"
    t.string   "policy_expiration_date"
    t.string   "value_of_death_benefit"
    t.text     "notes"
    t.integer  "user_id"
    t.integer  "contact_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "life_insurances", ["user_id"], name: "index_life_insurances_on_user_id", using: :btree

  create_table "newslatters", force: :cascade do |t|
    t.string   "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "obituaries", force: :cascade do |t|
    t.text     "memories_and_accomplishments"
    t.text     "note_and_instructions"
    t.integer  "user_id"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "obituaries", ["user_id"], name: "index_obituaries_on_user_id", using: :btree

  create_table "other_family_members", force: :cascade do |t|
    t.string   "related_to_you"
    t.text     "notes"
    t.integer  "user_id"
    t.integer  "contact_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "other_family_members", ["user_id"], name: "index_other_family_members_on_user_id", using: :btree

  create_table "other_letters", force: :cascade do |t|
    t.text     "body_letter"
    t.text     "notes"
    t.integer  "user_id"
    t.integer  "contact_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "other_letters", ["user_id"], name: "index_other_letters_on_user_id", using: :btree

  create_table "passports", force: :cascade do |t|
    t.string   "country_of_issue"
    t.string   "number"
    t.string   "expiration_date"
    t.string   "location"
    t.text     "notes"
    t.integer  "contact_id"
    t.integer  "user_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "passports", ["user_id"], name: "index_passports_on_user_id", using: :btree

  create_table "public_letters", force: :cascade do |t|
    t.text     "body_letter"
    t.text     "notes"
    t.integer  "user_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "public_letters", ["user_id"], name: "index_public_letters_on_user_id", using: :btree

  create_table "responsabilities", force: :cascade do |t|
    t.integer  "deputy_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "responsabilities_subcategories", force: :cascade do |t|
    t.integer  "responsability_id"
    t.integer  "subcategory_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "responsabilities_subcategories", ["responsability_id"], name: "index_responsabilities_subcategories_on_responsability_id", using: :btree
  add_index "responsabilities_subcategories", ["subcategory_id"], name: "index_responsabilities_subcategories_on_subcategory_id", using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "services", force: :cascade do |t|
    t.string   "type_of_service"
    t.string   "location"
    t.text     "note_and_instructions"
    t.integer  "user_id"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "services", ["user_id"], name: "index_services_on_user_id", using: :btree

  create_table "spouses", force: :cascade do |t|
    t.string   "status"
    t.text     "note"
    t.integer  "user_id"
    t.integer  "contact_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "spouses", ["user_id"], name: "index_spouses_on_user_id", using: :btree

  create_table "statuses", force: :cascade do |t|
    t.integer  "subcategory_id"
    t.integer  "user_id"
    t.string   "name",           default: "clear", null: false
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "statuses", ["subcategory_id"], name: "index_statuses_on_subcategory_id", using: :btree
  add_index "statuses", ["user_id"], name: "index_statuses_on_user_id", using: :btree

  create_table "subcategories", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.string   "permalink"
    t.integer  "category_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "permalink"
    t.string   "username"
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

  create_table "wills", force: :cascade do |t|
    t.string   "where_do_you_keep_notarized"
    t.string   "when_was_it_last_update"
    t.string   "where_is_it"
    t.text     "note_and_instructions"
    t.integer  "contact_id"
    t.integer  "user_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "wills", ["user_id"], name: "index_wills_on_user_id", using: :btree

  add_foreign_key "accounts", "users"
  add_foreign_key "advisors", "users"
  add_foreign_key "attorneys", "users"
  add_foreign_key "beneficiaries", "users"
  add_foreign_key "children", "users"
  add_foreign_key "contact_informations", "users"
  add_foreign_key "credit_cards", "users"
  add_foreign_key "day_care_personnels", "users"
  add_foreign_key "doctors", "users"
  add_foreign_key "drives_licenses", "users"
  add_foreign_key "emergencies", "users"
  add_foreign_key "executors", "users"
  add_foreign_key "guardians", "users"
  add_foreign_key "life_insurances", "users"
  add_foreign_key "obituaries", "users"
  add_foreign_key "other_family_members", "users"
  add_foreign_key "other_letters", "users"
  add_foreign_key "passports", "users"
  add_foreign_key "public_letters", "users"
  add_foreign_key "responsabilities_subcategories", "responsabilities"
  add_foreign_key "responsabilities_subcategories", "subcategories"
  add_foreign_key "services", "users"
  add_foreign_key "spouses", "users"
  add_foreign_key "statuses", "subcategories"
  add_foreign_key "statuses", "users"
  add_foreign_key "wills", "users"
end
