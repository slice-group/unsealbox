module ApplicationHelper
	def title(page_title)
		content_for(:title) { page_title }
	end
	
	def	meta_description(page_description)
		content_for(:description) { page_description }
	end

	def loggedin?
		current_user
	end

	def is_index?
		action_name.to_sym.eql?(:index)
	end

	def model
		controller.controller_path.classify.constantize
	end

	def header_information(&block)
		content_for(:header_information) { capture(&block) }
	end

	def link_change_status(subcategory)
		link_to change_status_path(subcategory.id), remote: true, "data-toggle" => "tooltip", "data-placement" => "right", title: "#{current_user.status_data(subcategory).fetch(:message)}" do
			content_tag :i, nil, class: "fa #{current_user.status_data(subcategory).fetch(:icon)}"
		end
	end

	def is_landing_page?
		controller_name.eql?("frontend") and action_name.eql?("index")
	end
end
