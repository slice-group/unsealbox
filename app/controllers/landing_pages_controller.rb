#Generado con Keppler.
class LandingPagesController < ApplicationController  
  before_filter :authenticate_user!
  layout 'admin/application'
  load_and_authorize_resource
  before_action :set_landing_page, only: [:show, :edit, :update, :destroy]

  # GET /landing_pages
  def index
    landing_pages = LandingPage.searching(@query).all
    @objects, @total = landing_pages.page(@current_page), landing_pages.size
    redirect_to landing_pages_path(page: @current_page.to_i.pred, search: @query) if !@objects.first_page? and @objects.size.zero?
  end

  # GET /landing_pages/1
  def show
  end

  # GET /landing_pages/new
  def new
    @landing_page = LandingPage.new
  end

  # GET /landing_pages/1/edit
  def edit
  end

  # POST /landing_pages
  def create
    @landing_page = LandingPage.new(landing_page_params)

    if @landing_page.save
      redirect_to @landing_page, notice: 'Landing page was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /landing_pages/1
  def update
    if @landing_page.update(landing_page_params)
      redirect_to @landing_page, notice: 'Landing page was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /landing_pages/1
  def destroy
    @landing_page.destroy
    redirect_to landing_pages_url, notice: 'Landing page was successfully destroyed.'
  end

  def destroy_multiple
    LandingPage.destroy redefine_ids(params[:multiple_ids])
    redirect_to landing_pages_path(page: @current_page, search: @query), notice: "Usuarios eliminados satisfactoriamente" 
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_landing_page
      @landing_page = LandingPage.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def landing_page_params
      params.require(:landing_page).permit(:title, :text_one, :text_two, :meta_tags, :google_analytics_track, :google_adwords_script, :background)
    end
end
