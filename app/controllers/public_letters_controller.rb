#Generado con Keppler.
class PublicLettersController < ApplicationController  
  before_filter :authenticate_user!
  layout 'layouts/frontend/application'
  before_action :set_public_letter, only: [:show, :edit, :update, :destroy]
  before_filter :set_subcategory

  # GET /public_letters
  def index
    @contacts = Contact.where(user_id: current_user.id).order(first_name: :asc)
    @public_letter = PublicLetter.new
    @public_letters = PublicLetter.where(user_id: current_user.id).page params[:page]
  end

  # GET /public_letters/1
  def show
  end

  # GET /public_letters/new
  def new
    @public_letter = PublicLetter.new
  end

  # GET /public_letters/1/edit
  def edit
  end

  # POST /public_letters
  def create
    @public_letter = PublicLetter.new(public_letter_params)
    @public_letter.user_id = current_user.id
    respond_to do |format|
      if @public_letter.save
        format.html { redirect_to public_letters_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'public_letter was successfully created.' }
        format.json { render :show, status: :created, location: @public_letter }
      else
        format.html { render :new }
        format.json { render json: @public_letter.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /public_letters/1
  def update
    case params[:commit]
    when "Save Information"
      respond_to do |format|
        if @public_letter.update(restructuring_parameters(@public_letter, public_letter_params))
          format.html { redirect_to public_letters_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'public_letter was successfully updated.' }
          format.json { render :show, status: :ok, location: @public_letter }
        else
          format.html { render :edit }
          format.json { render json: @public_letter.errors, status: :unprocessable_entity }
        end
      end
    when "Select this contact"
      @public_letter.update_attributes(contact_id: public_letter_params[:contact_id])
      redirect_to public_letters_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    when "Delete"
      @public_letter.update_attributes(contact_id: nil)
      redirect_to public_letters_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    end
  end

  # DELETE /public_letters/1
  def destroy    
    @public_letter.destroy
    respond_to do |format|
      format.html { redirect_to public_letters_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'public_letter was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_public_letter
      @public_letter = PublicLetter.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def public_letter_params
      params.require(:public_letter).permit(:body_letter, :notes, :user_id, contact_attributes: [:id, :first_name, :middle_name, :last_name, :birth_date, :phone_number, :email, :address, :city, :state, :zip, :country, :notes])
    end
end
