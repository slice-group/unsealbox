#Generado con Keppler.
class DrivesLicensesController < ApplicationController  
  before_filter :authenticate_user!
  layout 'layouts/frontend/application'
  before_action :set_drives_license, only: [:show, :edit, :update, :destroy]
  before_filter :set_subcategory

  # GET /drives_licenses
  def index
    @contacts = Contact.where(user_id: current_user.id).order(first_name: :asc)
    @drives_license = DrivesLicense.new
    @drives_licenses = DrivesLicense.where(user_id: current_user.id).page params[:page]
  end

  # GET /drives_licenses/1
  def show
  end

  # GET /drives_licenses/new
  def new
    @drives_license = DrivesLicense.new
  end

  # GET /drives_licenses/1/edit
  def edit
  end

  # POST /drives_licenses
  def create
    @drives_license = DrivesLicense.new(drives_license_params)
    @drives_license.user_id = current_user.id
    respond_to do |format|
      if @drives_license.save
        format.html { redirect_to drives_licenses_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'drives_license was successfully created.' }
        format.json { render :show, status: :created, location: @drives_license }
      else
        format.html { render :new }
        format.json { render json: @drives_license.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /drives_licenses/1
  def update
    case params[:commit]
    when "Save Information"
      respond_to do |format|
        if @drives_license.update(restructuring_parameters(@drives_license, drives_license_params))
          format.html { redirect_to drives_licenses_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'drives_license was successfully updated.' }
          format.json { render :show, status: :ok, location: @drives_license }
        else
          format.html { render :edit }
          format.json { render json: @drives_license.errors, status: :unprocessable_entity }
        end
      end
    when "Select this contact"
      @drives_license.update_attributes(contact_id: drives_license_params[:contact_id])
      redirect_to drives_licenses_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    when "Delete"
      @drives_license.update_attributes(contact_id: nil)
      redirect_to drives_licenses_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    end
  end

  # DELETE /drives_licenses/1
  def destroy    
    @drives_license.destroy
    respond_to do |format|
      format.html { redirect_to drives_licenses_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'drives_license was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_drives_license
      @drives_license = DrivesLicense.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def drives_license_params
      params.require(:drives_license).permit(:number, :country, :state, :expiration_date, :string, :location, :notes, :contact_id, :user_id, contact_attributes: [:id, :first_name, :middle_name, :last_name, :birth_date, :phone_number, :email, :address, :city, :state, :zip, :country, :notes])
    end
end
