#Generado con Keppler.
class BeneficiariesController < ApplicationController  
  before_filter :authenticate_user!
  layout 'layouts/frontend/application'
  before_action :set_beneficiary, only: [:show, :edit, :update, :destroy]
  before_filter :set_subcategory

  # GET /beneficiaries
  def index
    @contacts = Contact.where(user_id: current_user.id).order(first_name: :asc)
    @beneficiary = Beneficiary.new
    @beneficiaries = Beneficiary.where(user_id: current_user.id).page params[:page]
  end

  # GET /beneficiaries/1
  def show
  end

  # GET /beneficiaries/new
  def new
    @beneficiary = Beneficiary.new
  end

  # GET /beneficiaries/1/edit
  def edit
  end

  # POST /beneficiaries
  def create
    @beneficiary = Beneficiary.new(beneficiary_params)
    @beneficiary.user_id = current_user.id
    respond_to do |format|
      if @beneficiary.save
        format.html { redirect_to beneficiaries_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'beneficiary was successfully created.' }
        format.json { render :show, status: :created, location: @beneficiary }
      else
        format.html { render :new }
        format.json { render json: @beneficiary.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /beneficiaries/1
  def update
    case params[:commit]
    when "Save Information"
      respond_to do |format|
        if @beneficiary.update(restructuring_parameters(@beneficiary, beneficiary_params))
          format.html { redirect_to beneficiaries_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'beneficiary was successfully updated.' }
          format.json { render :show, status: :ok, location: @beneficiary }
        else
          format.html { render :edit }
          format.json { render json: @beneficiary.errors, status: :unprocessable_entity }
        end
      end
    when "Select this contact"
      @beneficiary.update_attributes(contact_id: beneficiary_params[:contact_id])
      redirect_to beneficiaries_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    when "Delete"
      @beneficiary.update_attributes(contact_id: nil)
      redirect_to beneficiaries_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    end
  end

  # DELETE /beneficiaries/1
  def destroy    
    @beneficiary.destroy
    respond_to do |format|
      format.html { redirect_to beneficiaries_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'beneficiary was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_beneficiary
      @beneficiary = Beneficiary.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def beneficiary_params
      params.require(:beneficiary).permit(:name, :notes, :user_id, :contact_id, contact_attributes: [:id, :first_name, :middle_name, :last_name, :birth_date, :phone_number, :email, :address, :city, :state, :zip, :country, :notes])
    end
end
