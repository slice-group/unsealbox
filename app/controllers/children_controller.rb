class ChildrenController < ApplicationController
  before_filter :authenticate_user!
  layout 'layouts/frontend/application'
  before_action :set_child, only: [:show, :edit, :update, :destroy]
  before_filter :set_subcategory

  # GET /children
  # GET /children.json
  def index
    @children = Child.where(user_id: current_user.id).page(params[:page])
    @child = Child.new       
    @contacts = Contact.where(user_id: current_user.id).order(first_name: :asc)
  end

  # GET /children/1
  # GET /children/1.json
  def show
  end

  # GET /children/new
  def new
    @child = Child.new
  end

  # GET /children/1/edit
  def edit
  end

  # POST /children
  # POST /children.json
  def create
    @child = Child.new(child_params)
    @child.user_id = current_user.id
    respond_to do |format|
      if @child.save
        format.html { redirect_to children_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Child was successfully created.' }
        format.json { render :show, status: :created, location: @child }
      else
        format.html { render :new }
        format.json { render json: @child.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /children/1
  # PATCH/PUT /children/1.json
  def update
    case params[:commit]
    when "Save Information"
      respond_to do |format|
        if @child.update(restructuring_parameters(@child, child_params))
          format.html { redirect_to children_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Child was successfully updated.' }
          format.json { render :show, status: :ok, location: @child }
        else
          format.html { render :edit }
          format.json { render json: @child.errors, status: :unprocessable_entity }
        end
      end
    when "Select this contact"
      @child.update_attributes(contact_id: child_params[:contact_id])
      redirect_to children_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    when "Delete"
      @child.update_attributes(contact_id: nil)
      redirect_to children_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    end
  end

  # DELETE /children/1
  # DELETE /children/1.json
  def destroy
    @child.destroy
    respond_to do |format|
      format.html { redirect_to children_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Child was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_child
      @child = Child.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def child_params
      params.require(:child).permit(:school, :health, :dentary, :note, :user_id, :contact_id,contact_attributes: [:id, :first_name, :middle_name, :last_name, :birth_date, :phone_number, :email, :address, :city, :state, :zip, :country, :notes])
    end
end
