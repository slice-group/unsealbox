#Generado con Keppler.
class WillsController < ApplicationController  
  before_filter :authenticate_user!
  layout 'layouts/frontend/application'
  before_action :set_will, only: [:show, :edit, :update, :destroy]
  before_filter :set_subcategory

  # GET /wills
  def index
    @contacts = Contact.where(user_id: current_user.id).order(first_name: :asc)
    @will = Will.new
    @wills = Will.where(user_id: current_user.id).page params[:page]
  end

  # GET /wills/1
  def show
  end

  # GET /wills/new
  def new
    @will = Will.new
  end

  # GET /wills/1/edit
  def edit
  end

  # POST /wills
  def create
    @will = Will.new(will_params)
    @will.user_id = current_user.id
    respond_to do |format|
      if @will.save
        format.html { redirect_to wills_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'will was successfully created.' }
        format.json { render :show, status: :created, location: @will }
      else
        format.html { render :new }
        format.json { render json: @will.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /wills/1
  def update
    case params[:commit]
    when "Save Information"
      respond_to do |format|
        if @will.update(restructuring_parameters(@will, will_params))
          format.html { redirect_to wills_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'will was successfully updated.' }
          format.json { render :show, status: :ok, location: @will }
        else
          format.html { render :edit }
          format.json { render json: @will.errors, status: :unprocessable_entity }
        end
      end
    when "Select this contact"
      @will.update_attributes(contact_id: will_params[:contact_id])
      redirect_to wills_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    when "Delete"
      @will.update_attributes(contact_id: nil)
      redirect_to wills_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    end
  end

  # DELETE /wills/1
  def destroy    
    @will.destroy
    respond_to do |format|
      format.html { redirect_to wills_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'will was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_will
      @will = Will.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def will_params
      params.require(:will).permit(:where_do_you_keep_notarized, :when_was_it_last_update, :where_is_it, :note_and_instructions, :contact_id, :user_id, contact_attributes: [:id, :first_name, :middle_name, :last_name, :birth_date, :phone_number, :email, :address, :city, :state, :zip, :country, :notes])
    end
end
