#Generado con Keppler.
class LifeInsurancesController < ApplicationController  
  before_filter :authenticate_user!
  layout 'layouts/frontend/application'
  before_action :set_life_insurance, only: [:show, :edit, :update, :destroy]
  before_filter :set_subcategory

  # GET /life_insurances
  def index
    @contacts = Contact.where(user_id: current_user.id).order(first_name: :asc)
    @life_insurance = LifeInsurance.new
    @life_insurances = LifeInsurance.where(user_id: current_user.id).page params[:page]
  end

  # GET /life_insurances/1
  def show
  end

  # GET /life_insurances/new
  def new
    @life_insurance = LifeInsurance.new
  end

  # GET /life_insurances/1/edit
  def edit
  end

  # POST /life_insurances
  def create
    @life_insurance = LifeInsurance.new(life_insurance_params)
    @life_insurance.user_id = current_user.id
    respond_to do |format|
      if @life_insurance.save
        format.html { redirect_to life_insurances_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'life_insurance was successfully created.' }
        format.json { render :show, status: :created, location: @life_insurance }
      else
        format.html { render :new }
        format.json { render json: @life_insurance.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /life_insurances/1
  def update
    case params[:commit]
    when "Save Information"
      respond_to do |format|
        if @life_insurance.update(restructuring_parameters(@life_insurance, life_insurance_params))
          format.html { redirect_to life_insurances_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'life_insurance was successfully updated.' }
          format.json { render :show, status: :ok, location: @life_insurance }
        else
          format.html { render :edit }
          format.json { render json: @life_insurance.errors, status: :unprocessable_entity }
        end
      end
    when "Select this contact"
      @life_insurance.update_attributes(contact_id: life_insurance_params[:contact_id])
      redirect_to life_insurances_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    when "Delete"
      @life_insurance.update_attributes(contact_id: nil)
      redirect_to life_insurances_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    end
  end

  # DELETE /life_insurances/1
  def destroy    
    @life_insurance.destroy
    respond_to do |format|
      format.html { redirect_to life_insurances_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'life_insurance was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_life_insurance
      @life_insurance = LifeInsurance.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def life_insurance_params
      params.require(:life_insurance).permit(:name_of_insurance_company, :location_of_original_policy, :type_of_policy, :what_is_the_policy_number, :policy_start_date, :policy_expiration_date, :value_of_death_benefit, :notes, :user_id, :contact_id, contact_attributes: [:id, :first_name, :middle_name, :last_name, :birth_date, :phone_number, :email, :address, :city, :state, :zip, :country, :notes])
    end
end
