#Generado con Keppler.
class DayCarePersonnelsController < ApplicationController  
  before_filter :authenticate_user!
  layout 'layouts/frontend/application'
  before_action :set_day_care_personnel, only: [:show, :edit, :update, :destroy]
  before_filter :set_subcategory

  # GET /day_care_personnels
  def index
    @contacts = Contact.where(user_id: current_user.id).order(first_name: :asc)
    @day_care_personnel = DayCarePersonnel.new
    @day_care_personnels = DayCarePersonnel.where(user_id: current_user.id).page params[:page]
  end

  # GET /day_care_personnels/1
  def show
  end

  # GET /day_care_personnels/new
  def new
    @day_care_personnel = DayCarePersonnel.new
  end

  # GET /day_care_personnels/1/edit
  def edit
  end

  # POST /day_care_personnels
  def create
    @day_care_personnel = DayCarePersonnel.new(day_care_personnel_params)
    @day_care_personnel.user_id = current_user.id
    respond_to do |format|
      if @day_care_personnel.save
        format.html { redirect_to day_care_personnels_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'day_care_personnel was successfully created.' }
        format.json { render :show, status: :created, location: @day_care_personnel }
      else
        format.html { render :new }
        format.json { render json: @day_care_personnel.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /day_care_personnels/1
  def update
    case params[:commit]
    when "Save Information"
      respond_to do |format|
        if @day_care_personnel.update(restructuring_parameters(@day_care_personnel, day_care_personnel_params))
          format.html { redirect_to day_care_personnels_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'day_care_personnel was successfully updated.' }
          format.json { render :show, status: :ok, location: @day_care_personnel }
        else
          format.html { render :edit }
          format.json { render json: @day_care_personnel.errors, status: :unprocessable_entity }
        end
      end
    when "Select this contact"
      @day_care_personnel.update_attributes(contact_id: day_care_personnel_params[:contact_id])
      redirect_to day_care_personnels_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    when "Delete"
      @day_care_personnel.update_attributes(contact_id: nil)
      redirect_to day_care_personnels_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    end
  end

  # DELETE /day_care_personnels/1
  def destroy    
    @day_care_personnel.destroy
    respond_to do |format|
      format.html { redirect_to day_care_personnels_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'day_care_personnel was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_day_care_personnel
      @day_care_personnel = DayCarePersonnel.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def day_care_personnel_params
      params.require(:day_care_personnel).permit(:type_of_day_care, :note_and_instructions, :contact_id, :user_id, contact_attributes: [:id, :first_name, :middle_name, :last_name, :birth_date, :phone_number, :email, :address, :city, :state, :zip, :country, :notes])
    end
end
