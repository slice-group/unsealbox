#Generado con Keppler.
class AttorneysController < ApplicationController  
  before_filter :authenticate_user!
  layout 'layouts/frontend/application'
  before_action :set_attorney, only: [:show, :edit, :update, :destroy]
  before_filter :set_subcategory

  # GET /attorneys
  def index
    @contacts = Contact.where(user_id: current_user.id).order(first_name: :asc)
    @attorney = Attorney.new
    @attorneys = Attorney.where(user_id: current_user.id).page params[:page]
  end

  # GET /attorneys/1
  def show
  end

  # GET /attorneys/new
  def new
    @attorney = Attorney.new
  end

  # GET /attorneys/1/edit
  def edit
  end

  # POST /attorneys
  def create
    @attorney = Attorney.new(attorney_params)
    @attorney.user_id = current_user.id
    respond_to do |format|
      if @attorney.save
        format.html { redirect_to attorneys_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'attorney was successfully created.' }
        format.json { render :show, status: :created, location: @attorney }
      else
        format.html { render :new }
        format.json { render json: @attorney.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /attorneys/1
  def update
    case params[:commit]
    when "Save Information"
      respond_to do |format|
        if @attorney.update(restructuring_parameters(@attorney, attorney_params))
          format.html { redirect_to attorneys_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'attorney was successfully updated.' }
          format.json { render :show, status: :ok, location: @attorney }
        else
          format.html { render :edit }
          format.json { render json: @attorney.errors, status: :unprocessable_entity }
        end
      end
    when "Select this contact"
      @attorney.update_attributes(contact_id: attorney_params[:contact_id])
      redirect_to attorneys_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    when "Delete"
      @attorney.update_attributes(contact_id: nil)
      redirect_to attorneys_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    end
  end

  # DELETE /attorneys/1
  def destroy    
    @attorney.destroy
    respond_to do |format|
      format.html { redirect_to attorneys_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'attorney was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_attorney
      @attorney = Attorney.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def attorney_params
      params.require(:attorney).permit(:type_of_attorney, :note_and_instructions, :user_id, :contact_id, contact_attributes: [:id, :first_name, :middle_name, :last_name, :birth_date, :phone_number, :email, :address, :city, :state, :zip, :country, :notes])
    end
end
