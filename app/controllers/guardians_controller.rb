#Generado con Keppler.
class GuardiansController < ApplicationController  
  before_filter :authenticate_user!
  layout 'layouts/frontend/application'
  before_action :set_guardian, only: [:show, :edit, :update, :destroy]
  before_filter :set_subcategory

  # GET /guardians
  def index
    @contacts = Contact.where(user_id: current_user.id).order(first_name: :asc)
    @guardian = Guardian.new
    @guardians = Guardian.where(user_id: current_user.id).page params[:page]
  end

  # GET /guardians/1
  def show
  end

  # GET /guardians/new
  def new
    @guardian = Guardian.new
  end

  # GET /guardians/1/edit
  def edit
  end

  # POST /guardians
  def create
    @guardian = Guardian.new(guardian_params)
    @guardian.user_id = current_user.id
    respond_to do |format|
      if @guardian.save
        format.html { redirect_to guardians_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'guardian was successfully created.' }
        format.json { render :show, status: :created, location: @guardian }
      else
        format.html { render :new }
        format.json { render json: @guardian.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /guardians/1
  def update
    case params[:commit]
    when "Save Information"
      respond_to do |format|
        if @guardian.update(restructuring_parameters(@guardian, guardian_params))
          format.html { redirect_to guardians_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'guardian was successfully updated.' }
          format.json { render :show, status: :ok, location: @guardian }
        else
          format.html { render :edit }
          format.json { render json: @guardian.errors, status: :unprocessable_entity }
        end
      end
    when "Select this contact"
      @guardian.update_attributes(contact_id: guardian_params[:contact_id])
      redirect_to guardians_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    when "Delete"
      @guardian.update_attributes(contact_id: nil)
      redirect_to guardians_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    end
  end

  # DELETE /guardians/1
  def destroy    
    @guardian.destroy
    respond_to do |format|
      format.html { redirect_to guardians_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'guardian was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_guardian
      @guardian = Guardian.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def guardian_params
      params.require(:guardian).permit(:type_of_guardian, :note_and_instructions_for_guardians, :contact_id, :user_id, contact_attributes: [:id, :first_name, :middle_name, :last_name, :birth_date, :phone_number, :email, :address, :city, :state, :zip, :country, :notes])
    end
end
