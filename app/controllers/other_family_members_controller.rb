#Generado con Keppler.
class OtherFamilyMembersController < ApplicationController  
  before_filter :authenticate_user!
  layout 'layouts/frontend/application'
  before_action :set_other_family_member, only: [:show, :edit, :update, :destroy]
  before_filter :set_subcategory

  # GET /other_family_members
  def index
    @contacts = Contact.where(user_id: current_user.id).order(first_name: :asc)
    @other_family_member = OtherFamilyMember.new
    @other_family_members = OtherFamilyMember.where(user_id: current_user.id).page params[:page]
  end

  # GET /other_family_members/1
  def show
  end

  # GET /other_family_members/new
  def new
    @other_family_member = OtherFamilyMember.new
  end

  # GET /other_family_members/1/edit
  def edit
  end

  # POST /other_family_members
  def create
    @other_family_member = OtherFamilyMember.new(other_family_member_params)
    @other_family_member.user_id = current_user.id
    respond_to do |format|
      if @other_family_member.save
        format.html { redirect_to other_family_members_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'other_family_member was successfully created.' }
        format.json { render :show, status: :created, location: @other_family_member }
      else
        format.html { render :new }
        format.json { render json: @other_family_member.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /other_family_members/1
  def update
    case params[:commit]
    when "Save Information"
      respond_to do |format|
        if @other_family_member.update(restructuring_parameters(@other_family_member, other_family_member_params))
          format.html { redirect_to other_family_members_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'other_family_member was successfully updated.' }
          format.json { render :show, status: :ok, location: @other_family_member }
        else
          format.html { render :edit }
          format.json { render json: @other_family_member.errors, status: :unprocessable_entity }
        end
      end
    when "Select this contact"
      @other_family_member.update_attributes(contact_id: other_family_member_params[:contact_id])
      redirect_to other_family_members_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    when "Delete"
      @other_family_member.update_attributes(contact_id: nil)
      redirect_to other_family_members_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    end
  end

  # DELETE /other_family_members/1
  def destroy    
    @other_family_member.destroy
    respond_to do |format|
      format.html { redirect_to other_family_members_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'other_family_member was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_other_family_member
      @other_family_member = OtherFamilyMember.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def other_family_member_params
      params.require(:other_family_member).permit(:related_to_you, :notes, :user_id, :contact_id, contact_attributes: [:id, :first_name, :middle_name, :last_name, :birth_date, :phone_number, :email, :address, :city, :state, :zip, :country, :notes])
    end
end
