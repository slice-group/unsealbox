#Generado con Keppler.
class SubcategoriesController < ApplicationController  
  before_filter :authenticate_user!
  before_filter :client_access_denied
  layout 'admin/application'
  load_and_authorize_resource
  before_action :set_subcategory, only: [:show, :edit, :update, :destroy]

  # GET /subcategories
  def index
    subcategories = Subcategory.searching(@query).all
    @objects, @total = subcategories.page(@current_page), subcategories.size
    redirect_to subcategories_path(page: @current_page.to_i.pred, search: @query) if !@objects.first_page? and @objects.size.zero?
  end

  # GET /subcategories/1
  def show
  end

  # GET /subcategories/new
  def new
    @subcategory = Subcategory.new
  end

  # GET /subcategories/1/edit
  def edit
  end

  # POST /subcategories
  def create
    @subcategory = Subcategory.new(subcategory_params)

    if @subcategory.save
      redirect_to @subcategory, notice: 'Subcategory was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /subcategories/1
  def update
    if @subcategory.update(subcategory_params)
      redirect_to @subcategory, notice: 'Subcategory was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /subcategories/1
  def destroy
    @subcategory.destroy
    redirect_to subcategories_url, notice: 'Subcategory was successfully destroyed.'
  end

  def destroy_multiple
    Subcategory.destroy redefine_ids(params[:multiple_ids])
    redirect_to subcategories_path(page: @current_page, search: @query), notice: "Usuarios eliminados satisfactoriamente" 
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_subcategory
      @subcategory = Subcategory.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def subcategory_params
      params.require(:subcategory).permit(:name, :description, :category_id)
    end
end
