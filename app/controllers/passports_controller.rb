#Generado con Keppler.
class PassportsController < ApplicationController  
  before_filter :authenticate_user!
  layout 'layouts/frontend/application'
  before_action :set_passport, only: [:show, :edit, :update, :destroy]
  before_filter :set_subcategory

  # GET /passports
  def index
    @contacts = Contact.where(user_id: current_user.id).order(first_name: :asc)
    @passport = Passport.new
    @passports = Passport.where(user_id: current_user.id).page params[:page]
  end

  # GET /passports/1
  def show
  end

  # GET /passports/new
  def new
    @passport = Passport.new
  end

  # GET /passports/1/edit
  def edit
  end

  # POST /passports
  def create
    @passport = Passport.new(passport_params)
    @passport.user_id = current_user.id
    respond_to do |format|
      if @passport.save
        format.html { redirect_to passports_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'passport was successfully created.' }
        format.json { render :show, status: :created, location: @passport }
      else
        format.html { render :new }
        format.json { render json: @passport.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /passports/1
  def update
    case params[:commit]
    when "Save Information"
      respond_to do |format|
        if @passport.update(restructuring_parameters(@passport, passport_params))
          format.html { redirect_to passports_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'passport was successfully updated.' }
          format.json { render :show, status: :ok, location: @passport }
        else
          format.html { render :edit }
          format.json { render json: @passport.errors, status: :unprocessable_entity }
        end
      end
    when "Select this contact"
      @passport.update_attributes(contact_id: passport_params[:contact_id])
      redirect_to passports_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    when "Delete"
      @passport.update_attributes(contact_id: nil)
      redirect_to passports_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    end
  end

  # DELETE /passports/1
  def destroy    
    @passport.destroy
    respond_to do |format|
      format.html { redirect_to passports_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'passport was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_passport
      @passport = Passport.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def passport_params
      params.require(:passport).permit(:country_of_issue, :number, :expiration_date, :location, :notes, :contact_id, :user_id, contact_attributes: [:id, :first_name, :middle_name, :last_name, :birth_date, :phone_number, :email, :address, :city, :state, :zip, :country, :notes])
    end
end
