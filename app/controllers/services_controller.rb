#Generado con Keppler.
class ServicesController < ApplicationController  
  before_filter :authenticate_user!
  layout 'layouts/frontend/application'
  before_action :set_service, only: [:show, :edit, :update, :destroy]
  before_filter :set_subcategory

  # GET /services
  def index
    @contacts = Contact.where(user_id: current_user.id).order(first_name: :asc)
    @service = Service.new
    @services = Service.where(user_id: current_user.id).page params[:page]
  end

  # GET /services/1
  def show
  end

  # GET /services/new
  def new
    @service = Service.new
  end

  # GET /services/1/edit
  def edit
  end

  # POST /services
  def create
    @service = Service.new(service_params)
    @service.user_id = current_user.id
    respond_to do |format|
      if @service.save
        format.html { redirect_to services_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'service was successfully created.' }
        format.json { render :show, status: :created, location: @service }
      else
        format.html { render :new }
        format.json { render json: @service.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /services/1
  def update
    case params[:commit]
    when "Save Information"
      respond_to do |format|
        if @service.update(restructuring_parameters(@service, service_params))
          format.html { redirect_to services_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'service was successfully updated.' }
          format.json { render :show, status: :ok, location: @service }
        else
          format.html { render :edit }
          format.json { render json: @service.errors, status: :unprocessable_entity }
        end
      end
    when "Select this contact"
      @service.update_attributes(contact_id: service_params[:contact_id])
      redirect_to services_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    when "Delete"
      @service.update_attributes(contact_id: nil)
      redirect_to services_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    end
  end

  # DELETE /services/1
  def destroy    
    @service.destroy
    respond_to do |format|
      format.html { redirect_to services_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'service was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_service
      @service = Service.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def service_params
      params.require(:service).permit(:type_of_service, :location, :note_and_instructions, :user_id, :contact_id, contact_attributes: [:id, :first_name, :middle_name, :last_name, :birth_date, :phone_number, :email, :address, :city, :state, :zip, :country, :notes])
    end
end
