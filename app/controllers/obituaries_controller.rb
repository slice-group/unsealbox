#Generado con Keppler.
class ObituariesController < ApplicationController  
  before_filter :authenticate_user!
  layout 'layouts/frontend/application'
  before_action :set_obituary, only: [:show, :edit, :update, :destroy]
  before_filter :set_subcategory

  # GET /obituaries
  def index
    @contacts = Contact.where(user_id: current_user.id).order(first_name: :asc)
    @obituary = Obituary.new
    @obituaries = Obituary.where(user_id: current_user.id).page params[:page]
  end

  # GET /obituaries/1
  def show
  end

  # GET /obituaries/new
  def new
    @obituary = Obituary.new
  end

  # GET /obituaries/1/edit
  def edit
  end

  # POST /obituaries
  def create
    @obituary = Obituary.new(obituary_params)
    @obituary.user_id = current_user.id
    respond_to do |format|
      if @obituary.save
        format.html { redirect_to obituaries_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'obituary was successfully created.' }
        format.json { render :show, status: :created, location: @obituary }
      else
        format.html { render :new }
        format.json { render json: @obituary.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /obituaries/1
  def update
    case params[:commit]
    when "Save Information"
      respond_to do |format|
        if @obituary.update(restructuring_parameters(@obituary, obituary_params))
          format.html { redirect_to obituaries_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'obituary was successfully updated.' }
          format.json { render :show, status: :ok, location: @obituary }
        else
          format.html { render :edit }
          format.json { render json: @obituary.errors, status: :unprocessable_entity }
        end
      end
    when "Select this contact"
      @obituary.update_attributes(contact_id: obituary_params[:contact_id])
      redirect_to obituaries_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    when "Delete"
      @obituary.update_attributes(contact_id: nil)
      redirect_to obituaries_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    end
  end

  # DELETE /obituaries/1
  def destroy    
    @obituary.destroy
    respond_to do |format|
      format.html { redirect_to obituaries_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'obituary was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_obituary
      @obituary = Obituary.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def obituary_params
      params.require(:obituary).permit(:memories_and_accomplishments, :note_and_instructions, :contact_id, :user_id, :contact_id, contact_attributes: [:id, :first_name, :middle_name, :last_name, :birth_date, :phone_number, :email, :address, :city, :state, :zip, :country, :notes])
    end
end
