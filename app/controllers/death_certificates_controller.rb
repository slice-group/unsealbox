#Generado con Keppler.
class DeathCertificatesController < ApplicationController  
  before_filter :authenticate_user!
  layout 'admin/application'
  load_and_authorize_resource
  before_action :set_death_certificate, only: [:show, :edit, :update, :destroy]

  # GET /death_certificates
  def index
    death_certificates = DeathCertificate.searching(@query).all
    @objects, @total = death_certificates.page(@current_page), death_certificates.size
    redirect_to death_certificates_path(page: @current_page.to_i.pred, search: @query) if !@objects.first_page? and @objects.size.zero?
  end

  # GET /death_certificates/1
  def show
  end

  # GET /death_certificates/new
  def new
    @death_certificate = DeathCertificate.new
  end

  # GET /death_certificates/1/edit
  def edit
  end

  # POST /death_certificates
  def create
    @death_certificate = DeathCertificate.new(death_certificate_params)

    if @death_certificate.save
      redirect_to @death_certificate, notice: 'Death certificate was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /death_certificates/1
  def update
    if @death_certificate.update(death_certificate_params)
      redirect_to @death_certificate, notice: 'Death certificate was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /death_certificates/1
  def destroy
    @death_certificate.destroy
    redirect_to death_certificates_url, notice: 'Death certificate was successfully destroyed.'
  end

  def destroy_multiple
    DeathCertificate.destroy redefine_ids(params[:multiple_ids])
    redirect_to death_certificates_path(page: @current_page, search: @query), notice: "Death certificate was successfully destroyed." 
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_death_certificate
      @death_certificate = DeathCertificate.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def death_certificate_params
      params.require(:death_certificate).permit(:death_date, :death_notification_id)
    end
end
