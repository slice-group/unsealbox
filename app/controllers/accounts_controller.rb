#Generado con Keppler.
class AccountsController < ApplicationController  
  before_filter :authenticate_user!
  layout 'layouts/frontend/application'
  before_action :set_account, only: [:show, :edit, :update, :destroy]
  before_filter :set_subcategory

  # GET /accounts
  def index
    @contacts = Contact.where(user_id: current_user.id).order(first_name: :asc)
    @account = Account.new
    @accounts = Account.where(user_id: current_user.id).page params[:page]
  end

  # GET /accounts/1
  def show
  end

  # GET /accounts/new
  def new
    @account = Account.new
  end

  # GET /accounts/1/edit
  def edit
  end

  # POST /accounts
  def create
    @account = Account.new(account_params)
    @account.user_id = current_user.id
    respond_to do |format|
      if @account.save
        format.html { redirect_to accounts_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'account was successfully created.' }
        format.json { render :show, status: :created, location: @account }
      else
        format.html { render :new }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /accounts/1
  def update
    case params[:commit]
    when "Save Information"
      respond_to do |format|
        if @account.update(restructuring_parameters(@account, account_params))
          format.html { redirect_to accounts_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'account was successfully updated.' }
          format.json { render :show, status: :ok, location: @account }
        else
          format.html { render :edit }
          format.json { render json: @account.errors, status: :unprocessable_entity }
        end
      end
    when "Select this contact"
      @account.update_attributes(contact_id: account_params[:contact_id])
      redirect_to accounts_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    when "Delete"
      @account.update_attributes(contact_id: nil)
      redirect_to accounts_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    end
  end

  # DELETE /accounts/1
  def destroy    
    @account.destroy
    respond_to do |format|
      format.html { redirect_to accounts_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'account was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_account
      @account = Account.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def account_params
      params.require(:account).permit(:type_of_account, :name_of_finalcial_institution, :account_number, :note_and_instructions, :contact_id, :user_id, contact_attributes: [:id, :first_name, :middle_name, :last_name, :birth_date, :phone_number, :email, :address, :city, :state, :zip, :country, :notes])
    end
end
