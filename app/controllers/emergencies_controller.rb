#Generado con Keppler.
class EmergenciesController < ApplicationController  
  before_filter :authenticate_user!
  layout 'layouts/frontend/application'
  before_action :set_emergency, only: [:show, :edit, :update, :destroy]
  before_filter :set_subcategory

  # GET /emergencies
  def index
    @contacts = Contact.where(user_id: current_user.id).order(first_name: :asc)
    @emergency = Emergency.new
    @emergencies = Emergency.where(user_id: current_user.id).page params[:page]
  end

  # GET /emergencies/1
  def show
  end

  # GET /emergencies/new
  def new
    @emergency = Emergency.new
  end

  # GET /emergencies/1/edit
  def edit
  end

  # POST /emergencies
  def create
    @emergency = Emergency.new(emergency_params)
    @emergency.user_id = current_user.id
    respond_to do |format|
      if @emergency.save
        format.html { redirect_to emergencies_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Emergency Contact was successfully created.' }
        format.json { render :show, status: :created, location: @emergency }
      else
        format.html { render :new }
        format.json { render json: @emergency.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /emergencies/1
  def update
    case params[:commit]
    when "Save Information"
      respond_to do |format|
        if @emergency.update(restructuring_parameters(@emergency, emergency_params))
          format.html { redirect_to emergencies_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Emergency Contact was successfully updated.' }
          format.json { render :show, status: :ok, location: @emergency }
        else
          format.html { render :edit }
          format.json { render json: @emergency.errors, status: :unprocessable_entity }
        end
      end
    when "Select this contact"
      @emergency.update_attributes(contact_id: emergency_params[:contact_id])
      redirect_to emergencies_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    when "Delete"
      @emergency.update_attributes(contact_id: nil)
      redirect_to emergencies_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    end
  end

  # DELETE /emergencies/1
  def destroy    
    @emergency.destroy
    respond_to do |format|
      format.html { redirect_to emergencies_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Emergency Contact was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_emergency
      @emergency = Emergency.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def emergency_params
      params.require(:emergency).permit(:relationship, :note, :contact_id, :user_id, contact_attributes: [:id, :first_name, :middle_name, :last_name, :birth_date, :phone_number, :email, :address, :city, :state, :zip, :country, :notes])
    end
end
