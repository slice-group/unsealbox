class AdminController < ApplicationController
	before_filter :authenticate_user!
	before_filter :client_access_denied
	
	layout 'admin/application'

	def index
		redirect_to '/admin/dashboard'
	end
end
