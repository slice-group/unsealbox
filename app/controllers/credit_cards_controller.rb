#Generado con Keppler.
class CreditCardsController < ApplicationController  
  before_filter :authenticate_user!
  layout 'layouts/frontend/application'
  before_action :set_credit_card, only: [:show, :edit, :update, :destroy]
  before_filter :set_subcategory

  # GET /credit_cards
  def index
    @contacts = Contact.where(user_id: current_user.id).order(first_name: :asc)
    @credit_card = CreditCard.new
    @credit_cards = CreditCard.where(user_id: current_user.id).page params[:page]
  end

  # GET /credit_cards/1
  def show
  end

  # GET /credit_cards/new
  def new
    @credit_card = CreditCard.new
  end

  # GET /credit_cards/1/edit
  def edit
  end

  # POST /credit_cards
  def create
    @credit_card = CreditCard.new(credit_card_params)
    @credit_card.user_id = current_user.id
    respond_to do |format|
      if @credit_card.save
        format.html { redirect_to credit_cards_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'credit_card was successfully created.' }
        format.json { render :show, status: :created, location: @credit_card }
      else
        format.html { render :new }
        format.json { render json: @credit_card.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /credit_cards/1
  def update
    case params[:commit]
    when "Save Information"
      respond_to do |format|
        if @credit_card.update(restructuring_parameters(@credit_card, credit_card_params))
          format.html { redirect_to credit_cards_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'credit_card was successfully updated.' }
          format.json { render :show, status: :ok, location: @credit_card }
        else
          format.html { render :edit }
          format.json { render json: @credit_card.errors, status: :unprocessable_entity }
        end
      end
    when "Select this contact"
      @credit_card.update_attributes(contact_id: credit_card_params[:contact_id])
      redirect_to credit_cards_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    when "Delete"
      @credit_card.update_attributes(contact_id: nil)
      redirect_to credit_cards_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    end
  end

  # DELETE /credit_cards/1
  def destroy    
    @credit_card.destroy
    respond_to do |format|
      format.html { redirect_to credit_cards_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'credit_card was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_credit_card
      @credit_card = CreditCard.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def credit_card_params
      params.require(:credit_card).permit(:type_of_card, :what_type_of_card_it, :card_number, :expiration_date, :note_and_instructions, :contact_id, :user_id, contact_attributes: [:id, :first_name, :middle_name, :last_name, :birth_date, :phone_number, :email, :address, :city, :state, :zip, :country, :notes])
    end
end
