class DeputiesController < ApplicationController
	before_filter :authenticate_user!
	layout 'layouts/frontend/application'
	
	def index
	end

	def responsabilities
	end

	def new
		@deputy = Deputy.new
		@deputy.build_responsability(user_id: current_user)
	end

	def create
		@deputy = Deputy.new(deputy_params)
		@deputy.responsability.user_id = current_user.id
    respond_to do |format|
      if @deputy.save
        format.html { redirect_to deputies_path, notice: 'Trustee was successfully created.' }
        format.json { render :show, status: :created, location: @deputy }
      else
        format.html { render :new }
        format.json { render json: @deputy.errors, status: :unprocessable_entity }
      end
    end
	end

	def update
		@responsability = Responsability.find(responsability_params[:id])
		@responsability.update(responsability_params)
	end

	def upload_document
		responsability = Responsability.find(params[:id])
		respond_to do |format|
      if responsability.update(responsability_params)
        format.html { redirect_to responsabilities_deputies_path, notice: 'Death certificate upload successfully created.' }
        format.json { render :show, status: :created, location: @deputy }
      else
        format.html { render :new }
        format.json { render json: @deputy.errors, status: :unprocessable_entity }
      end
    end
	end

	def destroy
		@deputy = Deputy.find(params[:id])
		case @deputy.permit_destroy? current_user
		when "user_action"
			@deputy.destroy
			redirect_to deputies_path, notice: 'Trustee was successfully removed.'	
		when "deputy_action"
			@deputy.destroy
			redirect_to responsabilities_deputies_path, notice: 'Trustee was successfully removed.'	
		else
			raise CanCan::AccessDenied.new("Not authorized") 
		end	
	end

	private

	# Never trust parameters from the scary internet, only allow the white list through.
  def deputy_params
    params.require(:deputy).permit(:first_name, :last_name, :email, :death_notification_id, responsability_attributes: [:id, subcategory_ids: []])
  end

  def responsability_params
  	params.require(:responsability).permit(:id, subcategory_ids: [], death_certificate_attributes: [:id, :document])
  end
end
