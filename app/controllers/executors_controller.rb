#Generado con Keppler.
class ExecutorsController < ApplicationController  
  before_filter :authenticate_user!
  layout 'layouts/frontend/application'
  before_action :set_executor, only: [:show, :edit, :update, :destroy]
  before_filter :set_subcategory

  # GET /executors
  def index
    @contacts = Contact.where(user_id: current_user.id).order(first_name: :asc)
    @executor = Executor.new
    @executors = Executor.where(user_id: current_user.id).page params[:page]
  end

  # GET /executors/1
  def show
  end

  # GET /executors/new
  def new
    @executor = Executor.new
  end

  # GET /executors/1/edit
  def edit
  end

  # POST /executors
  def create
    @executor = Executor.new(executor_params)
    @executor.user_id = current_user.id
    respond_to do |format|
      if @executor.save
        format.html { redirect_to executors_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'executor was successfully created.' }
        format.json { render :show, status: :created, location: @executor }
      else
        format.html { render :new }
        format.json { render json: @executor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /executors/1
  def update
    case params[:commit]
    when "Save Information"
      respond_to do |format|
        if @executor.update(restructuring_parameters(@executor, executor_params))
          format.html { redirect_to executors_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'executor was successfully updated.' }
          format.json { render :show, status: :ok, location: @executor }
        else
          format.html { render :edit }
          format.json { render json: @executor.errors, status: :unprocessable_entity }
        end
      end
    when "Select this contact"
      @executor.update_attributes(contact_id: executor_params[:contact_id])
      redirect_to executors_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    when "Delete"
      @executor.update_attributes(contact_id: nil)
      redirect_to executors_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    end
  end

  # DELETE /executors/1
  def destroy    
    @executor.destroy
    respond_to do |format|
      format.html { redirect_to executors_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'executor was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_executor
      @executor = Executor.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def executor_params
      params.require(:executor).permit(:type_of_executor, :note_and_instructions_for_executor, :contact_id, :user_id, contact_attributes: [:id, :first_name, :middle_name, :last_name, :birth_date, :phone_number, :email, :address, :city, :state, :zip, :country, :notes])
    end
end
