class FrontendController < ApplicationController
	before_filter :authenticate_user!, except: [:index]
	layout 'layouts/frontend/application'
  before_filter :verify_statuses, only: [:mytasks]
  before_filter :deputy_auth, only: [:shared_plan, :shared_information, :shared_plan_report]
	
  def index
    return redirect_to myunsealbox_path("my-life") if current_user
    @config = LandingPage.first
  	@newslatter = Newslatter.new
  end

  def myboxes
    @categories = Category.all
    @myplan = true
  end

  def get_my_plan_information
    @objects = params[:objects].constantize.where user_id: current_user.id
  end  

  def mytasks
  	@categories = Category.all
  end

  def get_subcategories
    @category = Category.find(params[:category_id])
  end
  
  def get_contact
    @subcategory = Subcategory.find(params[:subcategory_id])
    @object = params[:model_name].classify.constantize.find(params[:model_id])
    @contact = Contact.find(params[:contact_id]) unless params[:contact_id].eql?("nil")
  end

  def change_status
    @subcategory = Subcategory.find(params[:subcategory_id])
    current_user.change_status @subcategory
  end

  def my_plan_report
    @categories = Category.all
    respond_to do |format|
      format.pdf do
        render :pdf => "#{current_user.name}'s Boxes", :layout => 'frontend/pdf.html.haml'
      end
    end
  end

  #shared deputy information
  def shared_plan
    @categories = Category.all
  end

  def shared_information
    @objects = params[:objects].constantize.where user_id: @user.id    
  end

  def shared_plan_report
    @categories = Category.all
    respond_to do |format|
      format.pdf do
        render :pdf => "#{@user.name}'s Boxes", :layout => 'frontend/pdf.html.haml'
      end
    end
  end
  ###
  
  private

  def verify_statuses
    @category = Category.find_by_permalink(params[:category_permalink])
    @category.verify_statuses(current_user)
  end

  def deputy_auth
    @user = User.find_by_permalink(params[:user_permalink])
    if !(@user and @user.is_deputy? current_user)
      raise CanCan::AccessDenied.new("Not authorized")
    else
      deputy = @user.deputies.find_by_email(current_user.email)
      if deputy.death_notification? and !deputy.permission_to_read_boxes?
        raise CanCan::AccessDenied.new("Not authorized")
      end 
    end  
  end

end
