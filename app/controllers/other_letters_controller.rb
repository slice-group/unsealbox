#Generado con Keppler.
class OtherLettersController < ApplicationController  
  before_filter :authenticate_user!
  layout 'layouts/frontend/application'
  before_action :set_other_letter, only: [:show, :edit, :update, :destroy]
  before_filter :set_subcategory

  # GET /other_letters
  def index
    @contacts = Contact.where(user_id: current_user.id).order(first_name: :asc)
    @other_letter = OtherLetter.new
    @other_letters = OtherLetter.where(user_id: current_user.id).page params[:page]
  end

  # GET /other_letters/1
  def show
  end

  # GET /other_letters/new
  def new
    @other_letter = OtherLetter.new
  end

  # GET /other_letters/1/edit
  def edit
  end

  # POST /other_letters
  def create
    @other_letter = OtherLetter.new(other_letter_params)
    @other_letter.user_id = current_user.id
    respond_to do |format|
      if @other_letter.save
        format.html { redirect_to other_letters_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'other_letter was successfully created.' }
        format.json { render :show, status: :created, location: @other_letter }
      else
        format.html { render :new }
        format.json { render json: @other_letter.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /other_letters/1
  def update
    case params[:commit]
    when "Save Information"
      respond_to do |format|
        if @other_letter.update(restructuring_parameters(@other_letter, other_letter_params))
          format.html { redirect_to other_letters_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'other_letter was successfully updated.' }
          format.json { render :show, status: :ok, location: @other_letter }
        else
          format.html { render :edit }
          format.json { render json: @other_letter.errors, status: :unprocessable_entity }
        end
      end
    when "Select this contact"
      @other_letter.update_attributes(contact_id: other_letter_params[:contact_id])
      redirect_to other_letters_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    when "Delete"
      @other_letter.update_attributes(contact_id: nil)
      redirect_to other_letters_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    end
  end

  # DELETE /other_letters/1
  def destroy    
    @other_letter.destroy
    respond_to do |format|
      format.html { redirect_to other_letters_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'other_letter was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_other_letter
      @other_letter = OtherLetter.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def other_letter_params
      params.require(:other_letter).permit(:body_letter, :notes, :user_id, :contact_id, contact_attributes: [:id, :first_name, :middle_name, :last_name, :birth_date, :phone_number, :email, :address, :city, :state, :zip, :country, :notes])
    end
end
