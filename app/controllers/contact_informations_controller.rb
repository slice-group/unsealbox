#Generado con Keppler.
class ContactInformationsController < ApplicationController  
  before_filter :authenticate_user!
  layout 'layouts/frontend/application'
  before_action :set_contact_information, only: [:show, :edit, :update, :destroy]
  before_filter :set_subcategory

  # GET /contact_informations
  def index
    @contacts = Contact.where(user_id: current_user.id).order(first_name: :asc)
    @contact_information = ContactInformation.new
    @contact_informations = ContactInformation.where(user_id: current_user.id).page params[:page]
  end

  # GET /contact_informations/1
  def show
  end

  # GET /contact_informations/new
  def new
    @contact_information = ContactInformation.new
  end

  # GET /contact_informations/1/edit
  def edit
  end

  # POST /contact_informations
  def create
    @contact_information = ContactInformation.new(restructuring_parameters(@contact_information, contact_information_params))
    @contact_information.user_id = current_user.id
    respond_to do |format|
      if @contact_information.save
        format.html { redirect_to contact_informations_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'contact_information was successfully created.' }
        format.json { render :show, status: :created, location: @contact_information }
      else
        format.html { render :new }
        format.json { render json: @contact_information.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /contact_informations/1
  def update
    case params[:commit]
    when "Save Information"
      respond_to do |format|
        if @contact_information.update(restructuring_parameters(@contact_information, contact_information_params))
          format.html { redirect_to contact_informations_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'contact_information was successfully updated.' }
          format.json { render :show, status: :ok, location: @contact_information }
        else
          format.html { render :edit }
          format.json { render json: @contact_information.errors, status: :unprocessable_entity }
        end
      end
    when "Select this contact"
      @contact_information.update_attributes(contact_id: contact_information_params[:contact_id])
      redirect_to contact_informations_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    when "Delete"
      @contact_information.update_attributes(contact_id: nil)
      redirect_to contact_informations_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    end
  end

  # DELETE /contact_informations/1
  def destroy    
    @contact_information.destroy
    respond_to do |format|
      format.html { redirect_to contact_informations_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'contact_information was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contact_information
      @contact_information = ContactInformation.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def contact_information_params
      params.require(:contact_information).permit(:notes, :contact_id, :user_id, contact_attributes: [:id, :first_name, :middle_name, :last_name, :birth_date, :phone_number, :email, :address, :city, :state, :zip, :country, :notes])
    end
end
