#Generado con Keppler.
class AdvisorsController < ApplicationController  
  before_filter :authenticate_user!
  layout 'layouts/frontend/application'
  before_action :set_advisor, only: [:show, :edit, :update, :destroy]
  before_filter :set_subcategory

  # GET /advisors
  def index
    @contacts = Contact.where(user_id: current_user.id).order(first_name: :asc)
    @advisor = Advisor.new
    @advisors = Advisor.where(user_id: current_user.id).page params[:page]
  end

  # GET /advisors/1
  def show
  end

  # GET /advisors/new
  def new
    @advisor = Advisor.new
  end

  # GET /advisors/1/edit
  def edit
  end

  # POST /advisors
  def create
    @advisor = Advisor.new(advisor_params)
    @advisor.user_id = current_user.id
    respond_to do |format|
      if @advisor.save
        format.html { redirect_to advisors_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'advisor was successfully created.' }
        format.json { render :show, status: :created, location: @advisor }
      else
        format.html { render :new }
        format.json { render json: @advisor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /advisors/1
  def update
    case params[:commit]
    when "Save Information"
      respond_to do |format|
        if @advisor.update(restructuring_parameters(@advisor, advisor_params))
          format.html { redirect_to advisors_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'advisor was successfully updated.' }
          format.json { render :show, status: :ok, location: @advisor }
        else
          format.html { render :edit }
          format.json { render json: @advisor.errors, status: :unprocessable_entity }
        end
      end
    when "Select this contact"
      @advisor.update_attributes(contact_id: advisor_params[:contact_id])
      redirect_to advisors_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    when "Delete"
      @advisor.update_attributes(contact_id: nil)
      redirect_to advisors_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'Contact was successfully updated.'
    end
  end

  # DELETE /advisors/1
  def destroy    
    @advisor.destroy
    respond_to do |format|
      format.html { redirect_to advisors_path(@subcategory.category.permalink, @subcategory.permalink), notice: 'advisor was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_advisor
      @advisor = Advisor.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def advisor_params
      params.require(:advisor).permit(:type_of_advisor, :notes, :user_id, :contact_id, contact_attributes: [:id, :first_name, :middle_name, :last_name, :birth_date, :phone_number, :email, :address, :city, :state, :zip, :country, :notes])
    end
end
