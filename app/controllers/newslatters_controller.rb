#Generado con Keppler.
class NewslattersController < ApplicationController  
  before_filter :authenticate_user!, except: [:create]
  layout 'admin/application'
  load_and_authorize_resource except: [:create]
  before_action :set_newslatter, only: [:show, :edit, :update, :destroy]

  # GET /newslatters
  def index
    newslatters = Newslatter.searching(@query).all
    @objects, @total = newslatters.page(@current_page), newslatters.size
    redirect_to newslatters_path(page: @current_page.to_i.pred, search: @query) if !@objects.first_page? and @objects.size.zero?
  end

  # GET /newslatters/1
  def show
  end

  # GET /newslatters/new
  def new
    @newslatter = Newslatter.new
  end

  # GET /newslatters/1/edit
  def edit
  end

  # POST /newslatters
  def create
    @newslatter = Newslatter.new(newslatter_params)
    respond_to do |format|
      if @newslatter.save
        NewslatterMailer.send_message(@newslatter).deliver_now
        format.html { redirect_to root_path, notice: 'Thank you for leaving your e-mail address.' }
        format.json { render :show, status: :created, location: @newslatter }
      else
        format.html { redirect_to root_path, alert: 'We are sorry, you must enter a valid e-mail.' }
        format.json { render json: @newslatter.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /newslatters/1
  def update
    if @newslatter.update(newslatter_params)
      redirect_to @newslatter, notice: 'Newslatter was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /newslatters/1
  def destroy
    @newslatter.destroy
    redirect_to newslatters_url, notice: 'Newslatter was successfully destroyed.'
  end

  def destroy_multiple
    Newslatter.destroy redefine_ids(params[:multiple_ids])
    redirect_to newslatters_path(page: @current_page, search: @query), notice: "Usuarios eliminados satisfactoriamente" 
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_newslatter
      @newslatter = Newslatter.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def newslatter_params
      params.require(:newslatter).permit(:email)
    end
end
