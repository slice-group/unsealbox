class DayCarePersonnel < ActiveRecord::Base
  belongs_to :contact
  belongs_to :user
	accepts_nested_attributes_for :contact
end
