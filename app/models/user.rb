require 'elasticsearch/model'
class User < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  before_save :create_permalink, on: :create
  rolify
  validates_presence_of :name, :role_ids, :email
  has_many :statuses, dependent: :destroy 
  after_save :assing_statuses

  has_many :responsabilities
  has_many :deputies, :through => :responsabilities
  
  has_many :contacts, dependent: :destroy 
  has_many :doctors, dependent: :destroy 
  has_many :spouses, dependent: :destroy
  has_many :children, dependent: :destroy 
  has_many :emergencies, dependent: :destroy 
  has_many :life_insurances, dependent: :destroy 
  has_many :attorneys, dependent: :destroy 
  has_many :executor, dependent: :destroy 
  has_many :guardians, dependent: :destroy
  has_many :beneficiaries, dependent: :destroy
  has_many :obituaries, dependent: :destroy
  has_many :public_letters, dependent: :destroy
  has_many :other_letters, dependent: :destroy
  has_many :passports, dependent: :destroy
  has_many :drives_licenses, dependent: :destroy
  has_many :contact_informations, dependent: :destroy
  has_many :other_family_members, dependent: :destroy
  has_many :accounts, dependent: :destroy
  has_many :credit_cards, dependent: :destroy
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  after_commit on: [:update] do
    __elasticsearch__.index_document
  end

  def status_data(subcategory)
    case self.statuses.where(user_id: self.id, subcategory_id: subcategory.id).first.name.to_sym
    when :clear
      { icon: "fa-times-circle clear", message: "Change status to Done" }
    when :done
      { icon: "fa-check-circle done", message: "Change status to Empty"}
    when :ready
      { icon: "fa-check-circle ready", message: "This item is already in your Box. To change the status of an orange item, you must. clear the contents from your plan first"}
    end
  end

  def change_status(subcategory)
    status = self.statuses.where(user_id: self.id, subcategory_id: subcategory.id).first
    case status.name.to_sym
    when :clear
      status.update_attributes name: :done
    when :done
      status.update_attributes name: :clear
    end
  end

  def rol
    self.roles.first.name
  end

  def self.searching(query)
    if query
      self.search(self.query query).records.order(id: :desc)
    else
      self.order(id: :desc)
    end
  end

  def self.query(query)
    { query: { multi_match:  { query: query, fields: [:rol, :name, :email, :id] , operator: :and }  }, sort: { id: "desc" }, size: User.count }
  end

  #saber la pagina a la que pertenece
  def page(order = :id)
    ((self.class.order(order => :desc).pluck(order).index(self.send(order))+1).to_f / self.class.default_per_page).ceil
  end

  #armar indexado de elasticserch
  def as_indexed_json(options={})
    {
      id: self.id.to_s,
      email: self.email,
      name: self.name,
      rol: self.rol
    }.as_json
  end

  def get_deputy(deputy)
    self.deputies.where(email: deputy.email).first
  end

  def assigned_responsabilities
    Deputy.where(email: self.email)
  end

  def is_deputy?(deputy)
    !self.deputies.where(email: deputy.email).empty?
  end

  private

  def create_permalink
    self.permalink = self.name.downcase.parameterize+"-"+SecureRandom.hex(4)
  end

  def assing_statuses
    Subcategory.all.each do |subcategory|
      Status.create(user_id: self.id, subcategory_id: subcategory.id)
    end
  end

end

#User.import
