class DeathNotification < ActiveRecord::Base
	has_many :deputies	
	belongs_to :death_certificate

	def name_button
		if self.name.eql? "after i get in coma"
			"Is in coma?"
		elsif self.name.eql? "after i depart life"
			"Has departed life?"
		elsif self.name.eql? "after my funeral"
			"Is it on Funeral?"
		elsif self.name.eql? "1 month after my funeral"
			"Has passed 1 month from funeral?"
		end
	end
end
