class Contact < ActiveRecord::Base
	has_many :contact_informations, dependent: :destroy
	has_many :doctor
	has_many :life_insurances
	has_many :spouses
	has_many :children
	has_many :emergencies
	has_many :attorneys
	has_many :executor
	has_many :guardians
	has_many :beneficiaries
	has_many :public_letters
	has_many :other_letters
	has_many :passports
	has_many :drives_licenses
	has_many :other_family_members
	has_many :accounts
	has_many :credit_cards
	belongs_to :user

	def country_name
		country = ISO3166::Country[self.country]
		country.translations[I18n.locale.to_s] || country.name
	end
end
