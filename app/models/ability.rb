class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user (not logged in)
    if user.has_role? :admin

      # - user authorize -
      can [:delete, :show, :edit, :update, :create, :index, :destroy_multiple], User
      can :destroy, User do |u| !u.eql?(user) end

      # - category authorize -
      can [:show, :edit, :update, :index], Category

      # - subcategory authorize -
      can [:show, :edit, :update, :index], Subcategory

      # - newslatter authorize -
      can :manage, Newslatter

      # - dead_certificate authorize -
      can [:destroy, :show, :edit, :update, :index, :destroy_multiple], DeathCertificate

      # - landin_page authorize -
      can [:show, :edit, :update], LandingPage
      
    elsif user.has_role? :client
     
      
    end
    # The first argument to `can` is the action you are giving the user 
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. 
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/ryanb/cancan/wiki/Defining-Abilities
  end
end
