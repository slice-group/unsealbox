#Generado por keppler
require 'elasticsearch/model'
class Subcategory < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  before_save :create_permalink, on: :create
  belongs_to :category
  has_many :statuses
  has_and_belongs_to_many :responsabilities
  
  
  after_commit on: [:update] do
    __elasticsearch__.index_document
  end

  def self.searching(query)
    if query
      self.search(self.query query).records.order(id: :desc)
    else
      self.order(id: :desc)
    end
  end

  def self.query(query)
    { query: { multi_match:  { query: query, fields: [] , operator: :and }  }, sort: { id: "desc" }, size: self.count }
  end

  #armar indexado de elasticserch
  def as_indexed_json(options={})
    {
      id: self.id.to_s,
      name:  self.name.to_s,
      description:  self.description.to_s,
      permalink:  self.permalink.to_s,
      category_id:  self.category_id.to_s,
    }.as_json
  end

  def create_permalink
    self.permalink = self.name.downcase.parameterize
  end

  def shared_subcategory?(user, deputy, subcategory)
    deputy = user.get_deputy deputy
    deputy.responsability.subcategories.include? subcategory
  end

  def in_plan?(user)
    self.models_of_subcategory.each do |model|
      return true if model.where(user_id: user.id).count > 0
    end
    false
  end

  def self.information_empty?(user)
    all.each do |subcategory|
      subcategory.models_of_subcategory.each do |model|
        return false if model.where(user_id: user.id).count > 0
      end
    end
    true
  end

  def self.shared_information_empty?(user, deputy)
     deputy = user.get_deputy deputy
     deputy.responsability.subcategories.each do |subcategory|
      subcategory.models_of_subcategory.each do |model|
        return false if model.where(user_id: user.id).count > 0
      end
     end
     true
  end

  def models_of_subcategory
    case self.permalink
    when "about-me"
      [ContactInformation, Passport, DrivesLicense]
    when "my-family"
      [Spouse, Child, OtherFamilyMember]
    when "doctors"
      [Doctor]
    when "medical-assistants"
      [DayCarePersonnel]
    when "bank-information"
      [Account, CreditCard]
    when "my-life-insurance"
      [LifeInsurance, Beneficiary]
    when "financial-advisor"
      [Advisor]
    when "emergency-contacts"
      [Emergency]
    when "attorneys"
      [Attorney]
    when "will"
      [Will, Executor, Guardian]
    when "funeral-preference"
      [Obituary, Service]
    when "letter-to-family-friends"
      [PublicLetter, OtherLetter]
    else 
      []
    end
  end

  

end
#Subcategory.import
