class Responsability < ActiveRecord::Base
	belongs_to :deputy
	belongs_to :user
	has_one :death_certificate
	accepts_nested_attributes_for :death_certificate
	has_and_belongs_to_many :subcategories
end
