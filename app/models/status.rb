class Status < ActiveRecord::Base
  belongs_to :subcategory
  belongs_to :user

  def has_status?(name)
  	name == self.name.to_sym
  end
end
