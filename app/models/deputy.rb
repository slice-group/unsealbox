class Deputy < ActiveRecord::Base
	has_one :responsability, dependent: :destroy 
	belongs_to :death_notification
	accepts_nested_attributes_for :responsability
	validates_presence_of :first_name, :last_name, :email, :death_notification_id
	validate :user_registred?, :deputy_assigned?

	def permit_destroy?(user)
		if (self.responsability.user.id == user.id) 
			"user_action"
		elsif (self.responsability.deputy.email == user.email)
			"deputy_action"
		else
			false
		end
	end

	def user_registred?
		unless User.find_by_email(self.email)
      errors.add(:email, "the trustee has to be registered")
    end
	end

	def deputy_assigned?
		if self.allocator.is_deputy?(self)
			errors.add(:email, "this email has already been assigned to a trustee")
		end
	end

	def me
		User.find_by_email(self.email)
	end

	def allocator
		self.responsability.user
	end

	def death_certificate?
		!self.responsability.death_certificate.nil?
	end

	def death_notification?
		!self.responsability.death_certificate.death_notification.nil?
	end

	def permission_to_read_boxes?
		self.death_notification == self.responsability.death_certificate.death_notification
	end
end
