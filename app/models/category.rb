#Generado por keppler
require 'elasticsearch/model'
class Category < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  before_save :create_permalink, on: :create
  has_many :subcategories

  after_commit on: [:update] do
    __elasticsearch__.index_document
  end
  
  def self.searching(query)
    if query
      self.search(self.query query).records.order(id: :desc)
    else
      self.order(id: :desc)
    end
  end

  def self.query(query)
    { query: { multi_match:  { query: query, fields: [] , operator: :and }  }, sort: { id: "desc" }, size: self.count }
  end

  #armar indexado de elasticserch
  def as_indexed_json(options={})
    {
      id: self.id.to_s,
      name:  self.name.to_s,
      permalink:  self.permalink.to_s,
    }.as_json
  end

  def create_permalink
    self.permalink = self.name.downcase.parameterize
  end

  def verify_statuses(user)
    subcategories.each do |subcategory|
      status = Status.where(subcategory_id: subcategory.id, user_id: user.id).first

      if subcategory.in_plan?(user) and !status.has_status? :ready
        status.update_attributes(name: "ready")
      elsif !subcategory.in_plan?(user) and !status.has_status? :done
        status.update_attributes(name: "clear")
      end
    end
  end  

end
#Category.import
