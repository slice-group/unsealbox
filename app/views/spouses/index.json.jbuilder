json.array!(@spouses) do |spouse|
  json.extract! spouse, :id, :status, :note, :contact_id
  json.url spouse_url(spouse, format: :json)
end
