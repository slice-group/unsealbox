json.array!(@contacts) do |contact|
  json.extract! contact, :id, :first_name, :middle_name, :last_name, :birth_date, :phone_number, :email, :address, :city, :state, :zip, :country, :notes, :subcategory_id
  json.url contact_url(contact, format: :json)
end
