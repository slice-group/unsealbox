json.extract! @contact, :id, :first_name, :middle_name, :last_name, :birth_date, :phone_number, :email, :address, :city, :state, :zip, :country, :notes, :subcategory_id, :created_at, :updated_at
