json.array!(@children) do |child|
  json.extract! child, :id, :school, :health, :dentary, :note, :contact_id, :user_id
  json.url child_url(child, format: :json)
end
