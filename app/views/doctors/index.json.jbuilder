json.array!(@doctors) do |doctor|
  json.extract! doctor, :id, :type_of_doctor, :notes, :contact_id, :user_id
  json.url doctor_url(doctor, format: :json)
end
