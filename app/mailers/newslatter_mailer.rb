class NewslatterMailer < ActionMailer::Base
  default from: "info@unsealbox.com"

  def send_message(newslatter)
  	@newslatter = newslatter
    mail(to: @newslatter.email, subject: "UnsealBox")
  end
end